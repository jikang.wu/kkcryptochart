//
//  KKWebSocketMessage.h
//  KKCryptoChart
//
//  Created by Henry on 2022/2/24.
//

#import <Foundation/Foundation.h>

@interface KKWebSocketMessage : NSObject

@property(nonatomic, strong) NSString *body;
@property(nonatomic, strong) NSDictionary<NSString*, NSString*> *headers;
@property(nonatomic, strong) NSString *host;
@property(nonatomic, strong) NSNumber *isBase64;
@property(nonatomic, strong) NSString *method;
@property(nonatomic, strong) NSString *path;

@end

