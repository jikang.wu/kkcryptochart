//
//  KKWebSocketConfig.m
//  KKCryptoChart
//
//  Created by Henry on 2022/2/24.
//

#import "KKWebSocketConfig.h"

@implementation KKWebSocketConfig

-(instancetype)initWithAuthType:(NSString *)authType appCode:(NSString *)appCode contentType:(NSString *)contentType acceptType:(NSString *)acceptType {
    if (self = [super init]) {
        _authType = authType;
        _appCode = appCode;
        _contentType = contentType;
        _acceptType = acceptType;
    }
    return self;
}

@end
