//
//  KKWebSocketRoom.h
//  KKCryptoChart
//
//  Created by Henry on 2022/2/24.
//

#import <Foundation/Foundation.h>

@interface KKWebSocketRoom : NSObject

@property(nonatomic, strong) NSString *room;
@property(nonatomic, strong) NSString *symbol;
@property(nonatomic, strong) NSString *fromSymbol;
@property(nonatomic, strong) NSString *resolution;

-(instancetype)initWithRoom:(NSString *)room symbol:(NSString *)symbol fromSymbol:(NSString *)fromSymbol resolution:(NSString *)resolution;

@end

