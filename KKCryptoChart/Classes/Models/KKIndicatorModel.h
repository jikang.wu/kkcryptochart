//
//  KKIndicatorModel.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/2.
//

#import <Foundation/Foundation.h>
@class KKCryptoChartModel;

typedef NS_ENUM(NSInteger, KKIndicator) {
    KKIndicatorMA = 100,        // MA线
    KKIndicatorEMA,        // EMA线
    KKIndicatorBOLL,       // BOLL线
    KKIndicatorMACD = 104,   //MACD线
    KKIndicatorKDJ,        // KDJ线
    KKIndicatorRSI,         // RSI
    KKIndicatorWR,         // WR
    KKIndicatorVolume, // 交易量
    KKIndicatorTD, // TD
    KKIndicatorNone // 不显示指标线
};

typedef NS_ENUM(NSInteger, KKTDIndicatorType) {
    KKTDIndicatorTypeBuy = 100,
    KKTDIndicatorTypeSell,
    KKTDIndicatorTypeBuyNinth,
    KKTDIndicatorTypeBuyThirteenth,
    KKTDIndicatorTypeSellNinth,
    KKTDIndicatorTypeSellThirteenth
};

NS_ASSUME_NONNULL_BEGIN

@interface KKMACDModel : NSObject
@property (nonatomic, strong) NSNumber *DIFF;
@property (nonatomic, strong) NSNumber *DEA;
@property (nonatomic, strong) NSNumber *MACD;
+ (void)calMACDWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params;
@end

@interface KKKDJModel : NSObject
@property (nonatomic, strong) NSNumber *K;
@property (nonatomic, strong) NSNumber *D;
@property (nonatomic, strong) NSNumber *J;
@property (nonatomic, strong) NSNumber *RSV;
+ (void)calKDJWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params;
@end

@interface KKMAModel : NSObject
@property (nonatomic, strong) NSNumber *MA1;
@property (nonatomic, strong) NSNumber *MA2;
@property (nonatomic, strong) NSNumber *MA3;
+ (void)calMAWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params;
@end

@interface KKRSIModel : NSObject
@property (nonatomic, strong) NSNumber *RSI1;
@property (nonatomic, strong) NSNumber *RSI2;
@property (nonatomic, strong) NSNumber *RSI3;
+ (void)calRSIWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params;
@end

@interface KKBOLLModel : NSObject
@property (nonatomic, strong) NSNumber *UP;
@property (nonatomic, strong) NSNumber *MID;
@property (nonatomic, strong) NSNumber *LOW;
+ (void)calBOLLWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params;
@end

@interface KKWRModel : NSObject
@property (nonatomic, strong) NSNumber *WR1;
@property (nonatomic, strong) NSNumber *WR2;
+ (void)calWRWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params;
@end

@interface KKEMAModel : NSObject
@property (nonatomic, strong) NSNumber *EMA1;
@property (nonatomic, strong) NSNumber *EMA2;
@property (nonatomic, strong) NSNumber *EMA3;
+ (void)calEmaWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params;
@end

@interface KKTDModel : NSObject
@property (nonatomic, strong) NSNumber *buySetupIndex;
@property (nonatomic, strong) NSNumber *sellSetupIndex;
@property (nonatomic, strong) NSNumber *buyCountdownIndex;
@property (nonatomic, strong) NSNumber *sellCountdownIndex;
@property (nonatomic, assign) BOOL countdownIndexIsEqualToPreviousElement;
@property (nonatomic, assign) BOOL sellSetup;
@property (nonatomic, assign) BOOL buySetup;
@property (nonatomic, assign) BOOL sellSetupPerfection;
@property (nonatomic, assign) BOOL buySetupPerfection;
@property (nonatomic, assign) BOOL bearishFlip;
@property (nonatomic, assign) BOOL bullishFlip;
@property (nonatomic, strong) NSNumber *TDSTBuy;
@property (nonatomic, strong) NSNumber *TDSTSell;
@property (nonatomic, assign) BOOL countdownResetForTDST;
+ (void)calTDWithData:(NSArray<KKCryptoChartModel *> *)datas;
@end

NS_ASSUME_NONNULL_END
