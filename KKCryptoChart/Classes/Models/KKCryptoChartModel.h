//
//  KKCryptoChartModel.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/2.
//

#import <Foundation/Foundation.h>
#import "KKIndicatorModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface KKCryptoChartModel : NSObject

// 图表索引
@property (nonatomic, assign) NSInteger index;
// 前一个模型
@property (nonatomic, weak) KKCryptoChartModel *preModel;
// 盘口类型
@property (nonatomic, strong) NSString *coinCode;
// 时间类型
@property (nonatomic, strong) NSString *timeType;
// 时间戳
@property (nonatomic, strong) NSNumber *timestamp;
// 开盘价
@property (nonatomic, strong) NSNumber *open;
// 收盘价
@property (nonatomic, strong) NSNumber *close;
// 最高价
@property (nonatomic, strong) NSNumber *high;
// 最低价
@property (nonatomic, strong) NSNumber *low;
// 成交量
@property (nonatomic, strong) NSNumber *volume;

// 涨跌额
@property (nonatomic, strong) NSNumber *change;
// 涨跌幅
@property (nonatomic, strong) NSNumber *percentage;

@property (nonatomic, strong) KKMACDModel *MACD;
@property (nonatomic, strong) KKKDJModel *KDJ;
@property (nonatomic, strong) KKMAModel *MA;
@property (nonatomic, strong) KKEMAModel *EMA;
@property (nonatomic, strong) KKRSIModel *RSI;
@property (nonatomic, strong) KKBOLLModel *BOLL;
@property (nonatomic, strong) KKWRModel *WR;
@property (nonatomic, strong) KKTDModel *TD;

@end

NS_ASSUME_NONNULL_END
