//
//  KKCryptoChartDataManager.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/4.
//

#import "KKCryptoChartDataManager.h"
#import "NetworkRequestUtil.h"
#import "KKCryptoChartModel.h"
#import "KKKLineInitApi.h"
#import "BundleUtil.h"
#import "KKCryptoChartConstant.h"

@interface KKCryptoChartDataManager()

@property (nonatomic, strong) NSDictionary<NSNumber*, NSString*> *timeDict;
@property (nonatomic, strong) NSDictionary<NSNumber*, NSString*> *socketResolutionDict;

@end

@implementation KKCryptoChartDataManager

+(instancetype)shareManager {
    static KKCryptoChartDataManager * _shareManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareManager = [[super allocWithZone:NULL] init];
    });
    return _shareManager;
}

+(instancetype)allocWithZone:(struct _NSZone *)zone {
    return [KKCryptoChartDataManager shareManager];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _timeDict = @{
            @(KKCryptoChartTimeTypeOneMinute) : @"1",
            @(KKCryptoChartTimeTypeFiveMinutes) : @"5",
            @(KKCryptoChartTimeTypeFifteenMinutes) : @"15",
            @(KKCryptoChartTimeTypeThirtyMinutes) : @"30",
            @(KKCryptoChartTimeTypeOneHour) : @"60",
            @(KKCryptoChartTimeTypeFourHours) : @"4H",
            @(KKCryptoChartTimeTypeOneDay) : @"D",
            @(KKCryptoChartTimeTypeOneWeek) : @"W",
            @(KKCryptoChartTimeTypeOneMonth) : @"M",
            @(KKCryptoChartTimeTypeOneYear) : @"12M"
        };
        _socketResolutionDict = @{
            @(KKCryptoChartTimeTypeOneMinute) : @"P1m",
            @(KKCryptoChartTimeTypeFiveMinutes) : @"P5m",
            @(KKCryptoChartTimeTypeFifteenMinutes) : @"P15m",
            @(KKCryptoChartTimeTypeThirtyMinutes) : @"P30m",
            @(KKCryptoChartTimeTypeOneHour) : @"P1h",
            @(KKCryptoChartTimeTypeFourHours) : @"P4h",
            @(KKCryptoChartTimeTypeOneDay) : @"P1d",
            @(KKCryptoChartTimeTypeOneWeek) : @"P1w",
            @(KKCryptoChartTimeTypeOneMonth) : @"P1m",
            @(KKCryptoChartTimeTypeOneYear) : @"P1y"
        };
    }
    return self;
}

- (NSString *)getLocalizableStringTable {
    NSString *table = nil;
    switch (self.config.locale) {
        case KKCryptoChartLocaleTypeSimplifiedChinese:{
            table = @"ChineseSimplified";
        }
            break;
        case KKCryptoChartLocaleTypeEnglish:{
            table = @"English";
        }
            break;
        case KKCryptoChartLocaleTypeTraditionalChinese: {
            table = @"ChineseTraditional";
        }
            break;
        default:{
            table = @"English";
        }
            break;
    }
    return table;
}

- (NSString *)getLocalizableStringWithKey:(NSString *)key {
    if (!key || ![self getLocalizableStringTable]) {
        return nil;
    }
    return [[BundleUtil getBundle] localizedStringForKey:key value:nil table:[self getLocalizableStringTable]];
}

#pragma mark - fetch cloud data

/**
 * fetch cloud data from Kiki api
 */
- (void)fetchCloudDataWithCoinCode:(NSString *)coinCode timeType:(NSString *)timeType environment:(KKCryptoChartEnvironmentType)envType completionHandler:(void (^)(NSArray<KKCryptoChartModel *> *chartModels, NSString *error))completionHandler {
    __strong typeof(self) weakSelf = self;
    [NetworkRequestUtil fetchCloudDataWithUrl:[self setUpRequestApiWithCoinCode:coinCode timeType:timeType environment:envType] completionHandler:^(NSDictionary * _Nullable response, NSString * _Nullable error) {
        if (!error) {
            if (!response || [response isKindOfClass:[NSNull class]]) {
                if (completionHandler) {
                    completionHandler(nil, @"fetchCloudDataWithCoinCode response is invalid");
                }
                return;
            }
            if (![self isContainsKey:response.allKeys key:@"bars"]) {
                if (completionHandler) {
                    completionHandler(nil, @"fetchCloudDataWithCoinCode response is not contain key: [bars]");
                }
                return;
            }
            NSArray *bars = [response objectForKey:@"bars"];
            if (!bars || bars.count == 0) {
                if (completionHandler) {
                    completionHandler(nil, @"fetchCloudDataWithCoinCode bars is null");
                }
                return;
            }
            NSArray<KKCryptoChartModel *> *chartModels = [weakSelf handleChartsData:bars coinCode:coinCode timeType:timeType];
            if (chartModels.count > 1000) {
                chartModels = [chartModels subarrayWithRange:NSMakeRange(chartModels.count - 1000 - 1, 1000)];
            }
            KKLog(@"fetchCloudData coinCode=%@, timeType=%@, modelCount=%lu", coinCode, timeType, (unsigned long)chartModels.count);
            if (completionHandler) {
                completionHandler(chartModels, nil);
            }
        } else {
            if (completionHandler) {
                completionHandler(nil, error);
            }
        }
    }];
}

- (BOOL)isContainsKey:(NSArray *)allKeys key:(NSString *)key {
    if (!allKeys || allKeys.count == 0 || !key) {
        return NO;
    }
    for (NSString *comparedKey in allKeys) {
        if ([comparedKey isEqualToString:key]) {
            return YES;
        }
    }
    return NO;
}

- (NSString *)setUpRequestApiWithCoinCode:(NSString *)coinCode timeType:(NSString *)timeType environment:(KKCryptoChartEnvironmentType)envType {
    KKKLineInitApi *api = [[KKKLineInitApi alloc] init];
    api.symbol = coinCode;
    api.resolution = timeType;
    api.environment = envType;
    NSArray *times = [self setUpRequestApiTime:[self getTimeTypeEnum:timeType]];
    api.from = times[0];
    api.to = times[1];
    return [api getCompletedURL];
}

- (KKCryptoChartTimeType)getTimeTypeEnum:(NSString *)timeType {
    if (timeType == nil) {
        return KKCryptoChartTimeTypeOneMinute;
    }
    for (NSNumber *key in self.timeDict.allKeys) {
        if ([timeType isEqualToString:[self.timeDict objectForKey:key]]) {
            return key.integerValue;
        }
    }
    return KKCryptoChartTimeTypeOneMinute;
}

- (NSString *)getTimeString:(KKCryptoChartTimeType)timeType {
    return [self.timeDict objectForKey:@(timeType)];
}

- (NSString *)getSocketResolution:(KKCryptoChartTimeType)timeType {
    return [self.socketResolutionDict objectForKey:@(timeType)];
}


- (NSString *)getTimeStringFromSocketResolution:(NSString *)resolution {
    if (!resolution) {
        return nil;
    }
    KKCryptoChartTimeType timeType = KKCryptoChartTimeTypeOneMinute;
    for (NSNumber *key in self.socketResolutionDict.allKeys) {
        if ([resolution isEqualToString:[self.socketResolutionDict objectForKey:key]]) {
            timeType = key.integerValue;
            break;
        }
    }
    return [self getTimeString:timeType];
}

- (NSArray<NSString *> *)setUpRequestApiTime:(KKCryptoChartTimeType)timeType {
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
    NSArray *times = nil;
    switch (timeType) {
        case KKCryptoChartTimeTypeOneMinute:{
            comps.hour = -16;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
        case KKCryptoChartTimeTypeFiveMinutes:{
            comps.hour = -80;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
        case KKCryptoChartTimeTypeFifteenMinutes:{
            comps.hour = -240;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
        case KKCryptoChartTimeTypeThirtyMinutes:{
            comps.hour = -480;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
        case KKCryptoChartTimeTypeOneHour:{
            comps.day = -40;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
        case KKCryptoChartTimeTypeFourHours:{
            comps.day = -160;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
        case KKCryptoChartTimeTypeOneDay:{
            comps.year = -3;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
        case KKCryptoChartTimeTypeOneWeek:{
            comps.year = -20;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
        case KKCryptoChartTimeTypeOneMonth:{
            comps.year = -20;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
        case KKCryptoChartTimeTypeOneYear:{
            comps.year = -20;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
        default:{
            comps.hour = -8;
            NSDate *date = [calendar dateByAddingComponents:comps toDate:now options:NSCalendarMatchStrictly];
            NSString *from = [NSString stringWithFormat:@"%.f",[date timeIntervalSince1970] * 1000];
            NSString *to = [NSString stringWithFormat:@"%.f",[now timeIntervalSince1970] * 1000];
            times = @[from, to];
        }
            break;
    }
    return times;
}

/**
    get indicator cycles eg: ma-(7,25,99)
 */
- (nonnull NSArray *)getIndicatorTimeCycles:(KKIndicator)indicatorType {
    NSArray *cycles;
    switch (indicatorType) {
        case KKIndicatorMA:
        case KKIndicatorEMA:
            cycles = @[@"7",@"25",@"99"];
            break;
        case KKIndicatorBOLL:
            cycles = @[@"21",@"2"];
            break;
        case KKIndicatorMACD:
            cycles = @[@"12",@"26",@"9"];
            break;
        case KKIndicatorKDJ:
            cycles = @[@"9",@"3",@"3"];
            break;
        case KKIndicatorRSI:
            cycles = @[@"6",@"12",@"24"];
            break;
        case KKIndicatorWR:
            cycles = @[@"6",@"14"];
            break;
        case KKIndicatorTD:
        case KKIndicatorVolume:
        case KKIndicatorNone:
        default:
            cycles = @[];
            break;
    }
    return cycles;
}

// translate raw data to kk model
- (NSMutableArray<KKCryptoChartModel *> *)handleChartsData:(NSArray *)data coinCode:(NSString *)coinCode timeType:(NSString *)timeType {
    NSMutableArray *candleDatas = [[NSMutableArray alloc] init];
    for (NSString *bar in data) {
        NSArray *vals = [bar componentsSeparatedByString:@","];
        
        // e.g "1635487200,4331.1,4321.1,4345.23,4308.95,134.854095410" - "时间（秒），关，开，高，低，交易量"
        if (vals.count == 6) {
            KKCryptoChartModel *data = [[KKCryptoChartModel alloc] init];
            data.coinCode = coinCode;
            data.timeType = timeType;
            data.timestamp = [NSNumber numberWithLongLong:((NSString *)vals[0]).longLongValue];
            data.close = [NSNumber numberWithDouble:((NSString *)vals[1]).doubleValue];
            data.open = [NSNumber numberWithDouble:((NSString *)vals[2]).doubleValue];
            data.high = [NSNumber numberWithDouble:((NSString *)vals[3]).doubleValue];
            data.low = [NSNumber numberWithDouble:((NSString *)vals[4]).doubleValue];
            data.volume = [NSNumber numberWithDouble:((NSString *)vals[5]).doubleValue];
            data.change = [NSNumber numberWithInt:0];
            data.percentage = [NSNumber numberWithInt:0];
            [candleDatas addObject:data];
        }else if (vals.count == 8){
                KKCryptoChartModel *data = [[KKCryptoChartModel alloc] init];
                data.coinCode = coinCode;
                data.timeType = timeType;
                data.timestamp = [NSNumber numberWithLongLong:((NSString *)vals[0]).longLongValue];
                data.open = [NSNumber numberWithDouble:((NSString *)vals[1]).doubleValue];
                data.close = [NSNumber numberWithDouble:((NSString *)vals[2]).doubleValue];
                data.high = [NSNumber numberWithDouble:((NSString *)vals[3]).doubleValue];
                data.low = [NSNumber numberWithDouble:((NSString *)vals[4]).doubleValue];
                data.volume = [NSNumber numberWithDouble:((NSString *)vals[5]).doubleValue];
                data.change = [NSNumber numberWithDouble:((NSString *)vals[6]).doubleValue];
                data.percentage = [NSNumber numberWithDouble:((NSString *)vals[7]).doubleValue];
                [candleDatas addObject:data];
        }
    }
    [candleDatas sortWithOptions:NSSortConcurrent usingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        if ([((KKCryptoChartModel *)obj1).timestamp doubleValue] < [((KKCryptoChartModel *)obj2).timestamp doubleValue]) {
            return NSOrderedAscending;
        } else {
            return NSOrderedDescending;
        }
    }];
    [self calculateIndicatorData:candleDatas];
    return candleDatas;
}

- (void)calculateIndicatorData:(NSArray<KKCryptoChartModel *> *)candleDatas {
    if (!candleDatas || candleDatas.count == 0) {
        return;
    }
    [KKMAModel calMAWithData:candleDatas params:[self getIndicatorTimeCycles:KKIndicatorMA]];
    [KKEMAModel calEmaWithData:candleDatas params:[self getIndicatorTimeCycles:KKIndicatorEMA]];
    [KKBOLLModel calBOLLWithData:candleDatas params:[self getIndicatorTimeCycles:KKIndicatorBOLL]];
    [KKMACDModel calMACDWithData:candleDatas params:[self getIndicatorTimeCycles:KKIndicatorMACD]];
    [KKKDJModel calKDJWithData:candleDatas params:[self getIndicatorTimeCycles:KKIndicatorKDJ]];
    [KKRSIModel calRSIWithData:candleDatas params:[self getIndicatorTimeCycles:KKIndicatorRSI]];
    [KKWRModel calWRWithData:candleDatas params:[self getIndicatorTimeCycles:KKIndicatorWR]];
    [KKTDModel calTDWithData:candleDatas];
}

@end
