//
//  KKCryptoChartTabBar.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/9.
//

#define TAB_BAR_COUNT 6

#import <UIKit/UIKit.h>
@class TabbarSelectUtil;

NS_ASSUME_NONNULL_BEGIN

@protocol KKCryptoChartTabBarDelegate <NSObject>

@optional

- (void)didTabItemSelected:(NSInteger)index title:(NSString *)title;

@end

@interface KKCryptoChartTabBar : UIView

@property (nonatomic, assign) id<KKCryptoChartTabBarDelegate> delegate;

- (void)setUpTypes:(NSArray<NSNumber *> *)types titles:(NSArray<NSString *> *)titles;
- (void)updateMoreTabItem:(NSInteger)type title:(NSString *)title;
- (void)setupSuffixString:(NSString *)suffixString;
- (void)updateSelectorTabStatus;

@end

NS_ASSUME_NONNULL_END
