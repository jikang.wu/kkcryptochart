//
//  KKIndicatorDescription.h
//  KKCryptoChart
//
//  Created by apple on 2021/7/19.
//

#import <UIKit/UIKit.h>
#import "KKIndicatorModel.h"

NS_ASSUME_NONNULL_BEGIN

@class KKCryptoChartModel, KKCryptoChartGlobalConfig;
@interface KKIndicatorDescription : UIView

- (void)setupDescription:(KKCryptoChartModel *)model type:(KKIndicator)type config:(KKCryptoChartGlobalConfig *)config;

@end

NS_ASSUME_NONNULL_END
