//
//  KKCryptoChartDescription.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class KKCryptoChartModel,KKCryptoChartGlobalConfig;
@interface KKCryptoChartDescription : UIView

// init chart model and config
- (void)setUpWithChartModel:(KKCryptoChartModel *)model config:(KKCryptoChartGlobalConfig *)config;
// update chart description
- (void)updateDescription:(KKCryptoChartModel *)model;

@end

NS_ASSUME_NONNULL_END
