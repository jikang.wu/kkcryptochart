//
//  KKCryptoChart.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/1.
//

#import "KKCryptoChartView.h"
#import "KKCryptoChartDescription.h"
#import "KKCryptoChartModel.h"
#import "DateUtil.h"
#import "KKCryptoChartConstant.h"
#import "KKCryptoChartDataManager.h"
#import "KKCryptoChartGlobalConfig.h"
#import "KKIndicatorSelector.h"
#import "KKCryptoChartTabBar.h"
#import "KKCryptoMoreSelector.h"
#import "TDIndicatorValueFormatter.h"
#import "KKIndicatorDescription.h"
#import "KKCryptoChart-Swift.h"
#import "BundleUtil.h"
#import "KKWebSocket.h"
#import "KKCryptoChartAxisInfoLayer.h"


#define MAIN_VIEW_RATIO 2 / 3
#define CHART_VIEW_PADDING 0.0f
#define VOLUME_VIEW_RATIO 1 / 3
#define CANDLE_CHART_CANDLE_COUNT 45
#define CHART_MIN_VIEW_RANGE 20.0f

static const NSInteger KKCryptoChartTabBarMore = KKCryptoChartTimeTypeOneDay + 1001;
static const NSInteger KKCryptoChartTabBarIndicator = KKCryptoChartTimeTypeOneDay + 1002;
static const CGFloat KKCryptoChartXAxisOffset = 0.5f;

@interface KKCryptoChartView() <UIGestureRecognizerDelegate, ChartViewDelegate, KKIndicatorSelectorDelegate, KKCryptoChartTabBarDelegate, KKCryptoMoreSelectorDelegate, KKWebSocketDelegate>

@property (nonatomic, strong) CombinedChartView *mainChartView;
@property (nonatomic, strong) CombinedChartView *volumeChartView;
@property (nonatomic, strong) CombinedChartView *indicatorChartView;
@property (nonatomic, strong) KKCryptoChartTabBar *chartTabBar;
@property (nonatomic, strong) KKCryptoMoreSelector *moreSelector;
@property (nonatomic, strong) KKIndicatorSelector *indicatorSelector;
@property (nonatomic, strong) KKIndicatorDescription *mainIndicatorDesc;
@property (nonatomic, strong) KKIndicatorDescription *volumeIndicatorDesc;
@property (nonatomic, strong) KKIndicatorDescription *sideIndicatorDesc;
@property (nonatomic, strong) ChartYAxis *mainChartViewRightAxis;


// long press vertical line
@property (strong, nonatomic) UIView *verticalLine;
// long press horizontal line
@property (strong, nonatomic) UIView *horizontalLine;
@property (strong, nonatomic) KKCryptoChartDescription *chartDescription;
@property (strong, nonatomic) UIActivityIndicatorView *loadingView;
@property (nonatomic, assign) CGFloat chartViewHeight;
@property (nonatomic, assign) BOOL isShowIndicatorChartView;

@property (strong, nonatomic) CombinedChartData *mainData;
@property (strong, nonatomic) CombinedChartData *volumeData;
@property (strong, nonatomic) CombinedChartData *indicatorData;
@property (nonatomic, strong) NSArray<KKCryptoChartModel *> *chartModels;
@property (nonatomic, strong) KKCryptoChartDataManager *dataManager;
@property (nonatomic, assign) KKIndicator currentMainIndType;
@property (nonatomic, assign) KKIndicator currentSideIndType;

@property (nonatomic, strong) NSArray<NSNumber *> *tabBarTypes;
@property (nonatomic, strong) NSArray<NSString *> *tabBarTitles;
@property (nonatomic, strong) NSArray<NSNumber *> *moreSelectorTypes;
@property (nonatomic, strong) NSArray<NSString *> *moreSelectorTitles;

@property (nonatomic, strong) CAShapeLayer *horizontalLineLayer;
@property (nonatomic, strong) CAShapeLayer *verticalLineLayer;
@property (nonatomic, strong) CAShapeLayer *circleLayer;
@property (nonatomic, strong) CAShapeLayer *pointLayer;
@property (nonatomic, strong) KKCryptoChartInfoLayer *infoLayer;
// 点击点对应的交易量文本
@property (nonatomic, strong) KKCryptoChartAxisInfoLayer *volumeLayer;
// 点击点对应的价格文本
@property (nonatomic, strong) KKCryptoChartAxisInfoLayer *priceLayer;
// 当前价格线
@property (nonatomic, strong) ChartLimitLine *priceLine;
// 当前价格按钮
@property (nonatomic, strong) UIButton *priceButton;

@property (nonatomic, strong) KKWebSocket *webSocket;
@property (nonatomic, copy) NSString *fromSymbol;

@end

@implementation KKCryptoChartView

#pragma mark - lazy

- (CAShapeLayer *)horizontalLineLayer {
    if (!_horizontalLineLayer){
        _horizontalLineLayer = [[CAShapeLayer alloc]init];
        _horizontalLineLayer.strokeColor = CROSS_SHAPED_LINE_COLOR.CGColor;
        _horizontalLineLayer.lineWidth = 0.5;
    }
    return _horizontalLineLayer;
}

- (CAShapeLayer *)verticalLineLayer {
    if (!_verticalLineLayer){
        _verticalLineLayer = [[CAShapeLayer alloc]init];
        _verticalLineLayer.strokeColor = CROSS_SHAPED_LINE_COLOR.CGColor;
        _verticalLineLayer.lineWidth = 0.5;
    }
    return _verticalLineLayer;
}

- (CAShapeLayer *)circleLayer {
    if (!_circleLayer){
        _circleLayer = [[CAShapeLayer alloc]init];
        _circleLayer.frame = CGRectMake(0, 0, CROSS_SHAPED_CIRCLE_RADIUS, CROSS_SHAPED_CIRCLE_RADIUS);
        _circleLayer.fillColor = CROSS_SHAPED_CIRCLE_COLOR.CGColor;
    }
    return _circleLayer;
}

- (CAShapeLayer *)pointLayer {
    if (!_pointLayer){
        _pointLayer = [[CAShapeLayer alloc]init];
        _pointLayer.frame = CGRectMake(0, 0, CROSS_SHAPED_CIRCLE_POINT_RADIUS, CROSS_SHAPED_CIRCLE_POINT_RADIUS);
        _pointLayer.fillColor = CROSS_SHAPED_CIRCLE_POINT_COLOR.CGColor;
    }
    return _pointLayer;
}

- (KKCryptoChartInfoLayer *)infoLayer {
    if (!_infoLayer){
        _infoLayer = [KKCryptoChartInfoLayer defaltInfoLayer];
    }
    return _infoLayer;
}

- (KKCryptoChartAxisInfoLayer *)volumeLayer {
    if (!_volumeLayer) {
        _volumeLayer = [[KKCryptoChartAxisInfoLayer alloc] init];
    }
    return _volumeLayer;
}

- (KKCryptoChartAxisInfoLayer *)priceLayer {
    if (!_priceLayer) {
        _priceLayer = [[KKCryptoChartAxisInfoLayer alloc] init];
    }
    return _priceLayer;
}

#pragma mark - init UI

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initConstantData];
        [self initUI:self.frame];
        [self setUpUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initConstantData];
        [self initUI:frame];
        [self setUpUI];
    }
    return self;
}

- (void)initUI:(CGRect)frame {
    [self initTabBarUI:frame];
    [self initChartUI:frame];
    [self initDescriptionUI:frame];
    [self initLoadingUI:frame];
}

// init tabBar ui
- (void)initTabBarUI:(CGRect)frame {
    self.chartTabBar = [[KKCryptoChartTabBar alloc] init];
    self.chartTabBar.delegate = self;
    [self addSubview:self.chartTabBar];
}

- (void)initChartUI:(CGRect)frame {
    self.mainChartView = [[CombinedChartView alloc] init];
    [self addSubview:self.mainChartView];
    
    self.volumeChartView = [[CombinedChartView alloc] init];
    [self addSubview:self.volumeChartView];
    
    self.indicatorChartView = [[CombinedChartView alloc] init];
    [self addSubview:self.indicatorChartView];
}

// init description ui
- (void)initDescriptionUI:(CGRect)frame {
//    self.chartDescription = [[KKCryptoChartDescription alloc] init];
//    [self insertSubview:self.chartDescription aboveSubview:self.mainChartView];
    self.mainIndicatorDesc = [[KKIndicatorDescription alloc] init];
//    [self insertSubview:self.mainIndicatorDesc aboveSubview:self.mainChartView];
    [self addSubview:self.mainIndicatorDesc];
    
    self.volumeIndicatorDesc = [[KKIndicatorDescription alloc] init];
//    [self insertSubview:self.volumeIndicatorDesc aboveSubview:self.volumeChartView];
    [self addSubview:self.volumeIndicatorDesc];
    
    self.sideIndicatorDesc = [[KKIndicatorDescription alloc] init];
//    [self insertSubview:self.sideIndicatorDesc aboveSubview:self.indicatorChartView];
    [self addSubview:self.sideIndicatorDesc];
}

- (void)initLoadingUI:(CGRect)frame {
    if (@available(iOS 13.0, *)) {
        self.loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
    } else {
        self.loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    }
    [self addSubview:self.loadingView];
}

#pragma mark - dealloc

- (void)dealloc {
    if (self.webSocket) {
        [self.webSocket close];
        self.webSocket = nil;
    }
}

#pragma mark - layout

- (void)layoutSubviews {
    KKLog(@"KKCryptoChartView layoutSubviews x=%f, y=%f, width=%f, height=%f", self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    [super layoutSubviews];
    [self setViewsFrame:self.frame];
}

- (void)setViewsFrame:(CGRect)frame {
    KKLog(@"setViewsFrame KKCryptoChartView x=%f, y=%f, w=%f, h=%f", frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
    // tab bar
    CGFloat pixelRatio = [UIScreen mainScreen].bounds.size.width / 375.0f;
    [self.chartTabBar setFrame:CGRectMake(CHART_VIEW_PADDING * pixelRatio, 0, frame.size.width - (CHART_VIEW_PADDING * 2), TAB_BAR_BUTTON_HEIGHT)];

    // main chart
    self.chartViewHeight = frame.size.height - CGRectGetHeight(self.chartTabBar.frame);
    CGFloat mainChartH = self.chartViewHeight * MAIN_VIEW_RATIO;
    [self.mainChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.chartTabBar.frame), frame.size.width, mainChartH)];
    [self.mainIndicatorDesc setFrame:CGRectMake(CHART_VIEW_PADDING, CGRectGetMaxY(self.chartTabBar.frame), frame.size.width - 50, 12)];

    // side chart
    if (self.isShowIndicatorChartView) {
        [self.volumeChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.mainChartView.frame), frame.size.width, self.chartViewHeight * VOLUME_VIEW_RATIO * 0.5f)];
        [self.volumeIndicatorDesc setFrame:CGRectMake(CHART_VIEW_PADDING, CGRectGetMaxY(self.mainChartView.frame), frame.size.width - 50, 12)];

        [self.indicatorChartView setFrame:CGRectMake(self.volumeChartView.frame.origin.x, CGRectGetMaxY(self.volumeChartView.frame), self.volumeChartView.frame.size.width, self.chartViewHeight * VOLUME_VIEW_RATIO * 0.5f)];
        [self.sideIndicatorDesc setFrame:CGRectMake(CHART_VIEW_PADDING, CGRectGetMaxY(self.volumeChartView.frame), frame.size.width - 50, 12)];
    } else {
        [self.volumeChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.mainChartView.frame), frame.size.width, self.chartViewHeight - mainChartH)];
        [self.volumeIndicatorDesc setFrame:CGRectMake(CHART_VIEW_PADDING, CGRectGetMaxY(self.mainChartView.frame), frame.size.width - 50, 12)];

        [self.indicatorChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.volumeChartView.frame), frame.size.width, 0)];
        [self.sideIndicatorDesc setFrame:CGRectMake(CHART_VIEW_PADDING, CGRectGetMaxY(self.volumeChartView.frame), frame.size.width - 50, 0)];
    }
//    [self.chartDescription setFrame:CGRectMake(15, CGRectGetMinY(self.mainChartView.frame) + 5, frame.size.width - 50, 52)];
    [self.loadingView setFrame:CGRectMake((CGRectGetWidth(frame) / 2) - self.loadingView.frame.size.width, (CGRectGetHeight(frame) / 2) + (CGRectGetHeight(self.chartTabBar.frame) / 2) - self.loadingView.frame.size.height, self.loadingView.frame.size.width, self.loadingView.frame.size.height)];
    KKLog(@"setViewsFrame loadingView x=%f, y=%f, w=%f, h=%f", self.loadingView.frame.origin.x, self.loadingView.frame.origin.y, self.loadingView.frame.size.width, self.loadingView.frame.size.height);
}

- (void)changeInfoLayerFrame:(CGFloat )x{
    
    //取消动画
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    if (x > SCREEN_WIDTH/2) {
        self.infoLayer.frame = CGRectMake(CHART_VIEW_PADDING * 2, 50, self.infoLayer.frame.size.width, self.infoLayer.frame.size.height);
    }else{
        self.infoLayer.frame = CGRectMake(self.mainChartView.frame.size.width - (CHART_VIEW_PADDING * 2) - self.infoLayer.frame.size.width, 50, self.infoLayer.frame.size.width, self.infoLayer.frame.size.height);
    }
    [CATransaction commit];
}

#pragma mark - set up chart view

- (void)setUpUI {
    [self setUpMainChartView];
    [self setUpVolumeChartView];
    [self setUpIndicatorChartView];
    [self addGesture];
}

- (void)setUpMainChartView {
    // delegate
    self.mainChartView.delegate = self;
    
    // hightlight
    self.mainChartView.highlightPerTapEnabled = NO;
    self.mainChartView.highlightPerDragEnabled = NO;

    // background
    self.mainChartView.drawGridBackgroundEnabled = NO;
    self.mainChartView.drawBarShadowEnabled = NO;
    self.mainChartView.highlightFullBarEnabled = NO;

    // legend && description
    self.mainChartView.legend.enabled = NO;
    self.mainChartView.chartDescription.enabled = NO;
    
    // provided charts (candle behind line)
    self.mainChartView.drawOrder = @[@(CombinedChartDrawOrderCandle), @(CombinedChartDrawOrderLine)];
    
    // no data text
    self.mainChartView.noDataText = @"";
    
    // gesture properities
    self.mainChartView.dragEnabled = YES;
    [self.mainChartView setScaleXEnabled:YES];
    [self.mainChartView setScaleYEnabled:NO];
    self.mainChartView.autoScaleMinMaxEnabled = YES;
    [self.mainChartView.viewPortHandler setMinimumScaleX:0.1f];
    [self.mainChartView.viewPortHandler setMaximumScaleX:50];

    // axis
    self.mainChartView.leftAxis.enabled = NO;
    self.mainChartViewRightAxis = self.mainChartView.rightAxis;
    self.mainChartViewRightAxis.drawGridLinesEnabled = YES;
    self.mainChartViewRightAxis.drawAxisLineEnabled = NO;
    self.mainChartViewRightAxis.enabled = YES;
    self.mainChartViewRightAxis.minWidth = self.mainChartViewRightAxis.maxWidth = KK_CRYPTO_CHART_YAXIS_WIDTH;
    self.mainChartViewRightAxis.labelPosition = YAxisLabelPositionInsideChart;
    self.mainChartViewRightAxis.gridLineWidth = KK_CRYPTO_CHART_GRID_LINE_WIDTH;
    self.mainChartViewRightAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    self.mainChartViewRightAxis.labelTextColor = KK_CRYPTO_CHART_YAXIS_TEXT_COLOR;
    self.mainChartViewRightAxis.labelFont = KK_CRYPTO_CHART_YAXIS_TEXT_FONT;
    self.mainChartViewRightAxis.axisLineColor =KK_CRYPTO_CHART_AXIS_COLOR;


    ChartXAxis *xAxis = self.mainChartView.xAxis;
    xAxis.drawLabelsEnabled = NO;
    [xAxis setLabelCount: XAXIS_LABEL_COUNT force:YES];
    xAxis.drawGridLinesEnabled = YES;
    xAxis.drawAxisLineEnabled = NO;
    xAxis.gridLineWidth = KK_CRYPTO_CHART_GRID_LINE_WIDTH;
    xAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.avoidFirstLastClippingEnabled = YES;
    xAxis.drawLimitLinesBehindDataEnabled = YES;
    
    [self initPriceView];
}

- (void)initPriceView {
//    [NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:2];
    self.priceLine = [[ChartLimitLine alloc] init];
    self.priceLine.lineWidth = 1.0f;
    self.priceLine.lineDashPhase = 2.0f;
    self.priceLine.lineDashLengths = [NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil];
    [self.mainChartViewRightAxis addLimitLine:self.priceLine];

    self.priceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.priceButton.opaque = 0.8;
    [self.priceButton setTitleColor:KK_CRYPTO_CHART_PRICE_BUTTON_TITLE_COLOR forState:UIControlStateNormal];
    self.priceButton.titleLabel.font = KK_CRYPTO_CHART_YAXIS_TEXT_FONT;
    [self.priceButton addTarget:self action:@selector(priceButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainChartView addSubview:self.priceButton];
}

- (void)setUpVolumeChartView {
    // delegate
    self.volumeChartView.delegate = self;

    // hightlight
    self.volumeChartView.highlightPerTapEnabled = NO;
    self.volumeChartView.highlightPerDragEnabled = NO;
    
    // background
    self.volumeChartView.drawBarShadowEnabled = NO;
    self.volumeChartView.drawGridBackgroundEnabled = NO;
    self.volumeChartView.highlightFullBarEnabled = NO;

    // legend && description
    self.volumeChartView.legend.enabled = NO;
    self.volumeChartView.chartDescription.enabled = NO;
    
    
    // provided charts (candle behind line)
    self.volumeChartView.drawOrder = @[@(CombinedChartDrawOrderLine), @(CombinedChartDrawOrderBar)];

    // no data text
    self.volumeChartView.noDataText = @"";

    // gesture properities
    self.volumeChartView.dragEnabled = YES;
    [self.volumeChartView setScaleXEnabled:YES];
    [self.volumeChartView setScaleYEnabled:NO];
    self.volumeChartView.autoScaleMinMaxEnabled = YES;
    [self.volumeChartView.viewPortHandler setMinimumScaleX:0.1f];
    [self.volumeChartView.viewPortHandler setMaximumScaleX:50];
    
    // axis
    self.volumeChartView.leftAxis.enabled = NO;
    ChartYAxis *rightAxis = self.volumeChartView.rightAxis;
    rightAxis.enabled = YES;
    rightAxis.drawGridLinesEnabled = YES;
    rightAxis.drawAxisLineEnabled = NO;
    [rightAxis setLabelCount:2 force:YES];
    rightAxis.axisMinimum = 0.0;
    rightAxis.minWidth = rightAxis.maxWidth = KK_CRYPTO_CHART_YAXIS_WIDTH;
    rightAxis.labelPosition = YAxisLabelPositionInsideChart;
    rightAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    rightAxis.labelTextColor = KK_CRYPTO_CHART_YAXIS_TEXT_COLOR;
    rightAxis.labelFont = KK_CRYPTO_CHART_YAXIS_TEXT_FONT;
    rightAxis.axisLineColor =KK_CRYPTO_CHART_AXIS_COLOR;

    ChartXAxis *xAxis = self.volumeChartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    [xAxis setLabelCount: XAXIS_LABEL_COUNT force:YES];
    xAxis.drawLabelsEnabled = YES;
    xAxis.drawGridLinesEnabled = YES;
    xAxis.drawAxisLineEnabled = YES;
    xAxis.gridLineWidth = KK_CRYPTO_CHART_GRID_LINE_WIDTH;
    xAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    xAxis.labelTextColor = KK_CRYPTO_CHART_XAXIS_TEXT_COLOR;
    xAxis.labelFont = KK_CRYPTO_CHART_XAXIS_TEXT_FONT;
    xAxis.axisLineColor =KK_CRYPTO_CHART_AXIS_COLOR;
    xAxis.avoidFirstLastClippingEnabled = YES;
    xAxis.drawLimitLinesBehindDataEnabled = YES;
}

- (void)setUpIndicatorChartView {
    // delegate
    self.indicatorChartView.delegate = self;

    // hightlight
    self.indicatorChartView.highlightPerTapEnabled = NO;
    self.indicatorChartView.highlightPerDragEnabled = NO;
    
    // background
    self.indicatorChartView.drawBarShadowEnabled = NO;
    self.indicatorChartView.drawGridBackgroundEnabled = NO;
    self.indicatorChartView.highlightFullBarEnabled = NO;

    // legend && description
    self.indicatorChartView.legend.enabled = NO;
    self.indicatorChartView.chartDescription.enabled = NO;
    
    // provided charts (bar behind line)
    self.indicatorChartView.drawOrder = @[@(CombinedChartDrawOrderBar), @(CombinedChartDrawOrderLine)];
    
    // no data text
    self.indicatorChartView.noDataText = @"";

    // gesture properities
    self.indicatorChartView.dragEnabled = YES;
    [self.indicatorChartView setScaleXEnabled:YES];
    [self.indicatorChartView setScaleYEnabled:NO];
    self.indicatorChartView.autoScaleMinMaxEnabled = YES;
    [self.indicatorChartView.viewPortHandler setMinimumScaleX:0.1f];
    [self.indicatorChartView.viewPortHandler setMaximumScaleX:50];
    
    // axis
    self.indicatorChartView.leftAxis.enabled = NO;
    ChartYAxis *rightAxis = self.indicatorChartView.rightAxis;
    rightAxis.enabled = YES;
    rightAxis.drawGridLinesEnabled = YES;
    rightAxis.drawAxisLineEnabled = NO;
    [rightAxis setLabelCount:2 force:YES];
    rightAxis.axisMinimum = 0.0;
    rightAxis.minWidth = rightAxis.maxWidth = KK_CRYPTO_CHART_YAXIS_WIDTH;
    rightAxis.labelPosition = YAxisLabelPositionInsideChart;
    rightAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    rightAxis.labelTextColor = KK_CRYPTO_CHART_YAXIS_TEXT_COLOR;
    rightAxis.labelFont = KK_CRYPTO_CHART_YAXIS_TEXT_FONT;
    rightAxis.axisLineColor =KK_CRYPTO_CHART_AXIS_COLOR;

    ChartXAxis *xAxis = self.indicatorChartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    [xAxis setLabelCount: XAXIS_LABEL_COUNT force:YES];
    xAxis.drawLabelsEnabled = YES;
    xAxis.drawGridLinesEnabled = YES;
    xAxis.drawAxisLineEnabled = YES;
    xAxis.gridLineWidth = KK_CRYPTO_CHART_GRID_LINE_WIDTH;
    xAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    xAxis.labelTextColor = KK_CRYPTO_CHART_XAXIS_TEXT_COLOR;
    xAxis.labelFont = KK_CRYPTO_CHART_XAXIS_TEXT_FONT;
    xAxis.axisLineColor =KK_CRYPTO_CHART_AXIS_COLOR;
    xAxis.avoidFirstLastClippingEnabled = YES;
    xAxis.drawLimitLinesBehindDataEnabled = YES;
}


- (void)addGesture {
    [self addTouchGesture];
//    [self addLongGesture];
}

- (void)addTouchGesture {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:nil];
    tapGesture.delegate = self;
    [self addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *mainChartTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(updateInfoLayer:)];
    [self.mainChartView addGestureRecognizer:mainChartTap];

    // 取消成交量图点击
//    UITapGestureRecognizer *volumeChartTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(updateInfoLayer:)];
//    [self.volumeChartView addGestureRecognizer:volumeChartTap];
}

/**
 * add long press listener
 */
- (void)addLongGesture {
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPress:)];
    [self addGestureRecognizer:longPressGesture];
}

#pragma mark - set data

- (void)initConstantData {
    self.dataManager = [KKCryptoChartDataManager shareManager];
    self.moreSelectorTypes = @[@(KKCryptoChartTimeTypeOneMinute), @(KKCryptoChartTimeTypeFiveMinutes), @(KKCryptoChartTimeTypeThirtyMinutes), @(KKCryptoChartTimeTypeOneWeek), @(KKCryptoChartTimeTypeOneMonth), @(KKCryptoChartTimeTypeOneYear)];

    self.currentMainIndType = KKIndicatorNone;
    self.currentSideIndType = KKIndicatorNone;
    self.isShowIndicatorChartView = NO;
    self.fromSymbol = @"";
}

- (void)initUIStrings {
    self.tabBarTypes = @[@(KKCryptoChartTimeTypeFifteenMinutes), @(KKCryptoChartTimeTypeOneHour), @(KKCryptoChartTimeTypeFourHours), @(KKCryptoChartTimeTypeOneDay), @(KKCryptoChartTabBarMore), @(KKCryptoChartTabBarIndicator)];

    self.tabBarTitles = @[
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_fifteen_minutes"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_hour"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_four_hours"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_day"],
        [NSString stringWithFormat:@"%@%@", [self.dataManager getLocalizableStringWithKey:@"chart_tab_bar_more"], [self.dataManager getLocalizableStringWithKey:@"chart_tab_bar_right_triangle"]],
        [NSString stringWithFormat:@"%@%@", [self.dataManager getLocalizableStringWithKey:@"chart_tab_bar_indicator"], [self.dataManager getLocalizableStringWithKey:@"chart_tab_bar_right_triangle"]]];

    self.moreSelectorTitles = @[
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_minute"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_five_minutes"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_thirty_minutes"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_week"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_month"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_year"]];

}

/**
 call after dataManager set config
 */
- (void)setUpSelectorData {
    [self initUIStrings];
    [self.chartTabBar setupSuffixString:[[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_tab_bar_right_triangle"]];
    [self.chartTabBar setUpTypes:self.tabBarTypes titles:self.tabBarTitles];
    
    //设置more的初始值 title
    if ([[TabbarSelectUtil share]getTabbarSelectBtnTag] == KKCryptoChartTabBarMore){
        NSInteger moreType = [[TabbarSelectUtil share]getTabbarSelectMoreBtnTag];
        if ([self.moreSelectorTypes containsObject:[NSNumber numberWithInteger:moreType]]){
            NSInteger index = [self.moreSelectorTypes indexOfObject:[NSNumber numberWithInteger:moreType]];
            NSString *title = self.moreSelectorTitles[index];
            [self.chartTabBar updateMoreTabItem:KKCryptoChartTabBarMore title:title];
        }
    }
    
    if (self.moreSelector) {
        [self.moreSelector setUpTypes:self.moreSelectorTypes titles:self.moreSelectorTitles];
    }
}

- (void)setConfig:(KKCryptoChartGlobalConfig *)config {
    _config = config;
    
    // 设置chartview的config.timeType 的初始配置
    NSInteger btnType = [[TabbarSelectUtil share]getTabbarSelectBtnTag];
    if (btnType != 0){
        if (btnType == KKCryptoChartTabBarMore){
            btnType = [[TabbarSelectUtil share]getTabbarSelectMoreBtnTag];
        }
        NSString *time = [[KKCryptoChartDataManager shareManager] getTimeString:btnType];
        config.timeType = time;
        _config = config;
    }
    
    //设置 IndicatorSelector 的 main和sub 初始值
    NSInteger mainType = [[TabbarSelectUtil share]getTabbarIndicatorMainBtnTag];
    if (mainType != 0){
        [self indicatorSelectorMainChartValueChanged:mainType];
    }
    
    NSInteger subType = [[TabbarSelectUtil share]getTabbarIndicatorSubBtnTag];
    if (subType != 0){
        [self indicatorSelectorSideChartValueChanged:subType];
    }
    
    [self fetchCloudData:config];

    [self setupWebSocketData:config];
}

- (void)updateConfig:(KKCryptoChartGlobalConfig *)config {
    [self.mainChartView clear];
    [self.volumeChartView clear];
    [self.indicatorChartView clear];
    [self fetchCloudData:config];
    [self setupWebSocketData:config];
}

- (void)setUpChartModels:(NSArray<KKCryptoChartModel *> *)models config:(KKCryptoChartGlobalConfig *)config {
    [self generateChartData:models];

    // adjust xAxis min and max
    [self adjustAxis:models];
    
    // description
//    [self.chartDescription setUpWithChartModel:models.lastObject config:config];
    [self setupDescription:models.lastObject config:config];

    // visible range (for limit candle width)
    [self.mainChartView setVisibleXRangeMinimum:CHART_MIN_VIEW_RANGE];
    [self.volumeChartView setVisibleXRangeMinimum:CHART_MIN_VIEW_RANGE];
//    [self.indicatorChartView setVisibleXRangeMinimum:CHART_MIN_VIEW_RANGE];
    
    // zoom
    [self.mainChartView zoomWithScaleX:[self calMaxScale:models.count] / self.mainChartView.scaleX  scaleY:0 x:0 y:0];
    [self.volumeChartView zoomWithScaleX:[self calMaxScale:models.count] / self.volumeChartView.scaleX scaleY:0 x:0 y:0];
    [self.indicatorChartView zoomWithScaleX:[self calMaxScale:models.count] / self.indicatorChartView.scaleX scaleY:0 x:0 y:0];
    KKLog(@"current mainChartView scaleX = %f, volumeChartView scaleX = %f, scaleX scaleX = %f", self.mainChartView.scaleX, self.volumeChartView.scaleX, self.indicatorChartView.scaleX);

    // move to right

    [self.mainChartView.data notifyDataChanged];
    [self.mainChartView notifyDataSetChanged];

    [self.volumeChartView.data notifyDataChanged];
    [self.volumeChartView notifyDataSetChanged];

    if (self.isShowIndicatorChartView) {
        [self.indicatorChartView.data notifyDataChanged];
        [self.indicatorChartView notifyDataSetChanged];
    }
    [self chartMoveToRight:models isAnimated:NO];
    
    // refresh price line
    [self refreshPriceLine:models.lastObject];
}

- (void)setupDescription:(KKCryptoChartModel *)model config:(KKCryptoChartGlobalConfig *)config {
    [self.mainIndicatorDesc setupDescription:model type:self.currentMainIndType config:config];
    [self.volumeIndicatorDesc setupDescription:model type:KKIndicatorVolume config:config];
    [self.sideIndicatorDesc setupDescription:model type:self.currentSideIndType config:config];
    [self.infoLayer setupAlertValueWithModel:model config:config];
}

- (void)chartMoveToRight:(NSArray<KKCryptoChartModel *> *)models isAnimated:(BOOL)isAnimated {
    if (isAnimated) {
        [self.mainChartView moveViewToAnimatedWithXValue:models.count - 1 yValue:0 axis:AxisDependencyRight duration:1.0];
        [self.volumeChartView moveViewToAnimatedWithXValue:models.count - 1 yValue:0 axis:AxisDependencyRight duration:1.0];
        [self.indicatorChartView moveViewToAnimatedWithXValue:models.count - 1 yValue:0 axis:AxisDependencyRight duration:1.0];
    } else {
        [self.mainChartView moveViewToX:models.count - 1];
        [self.volumeChartView moveViewToX:models.count - 1];
        [self.indicatorChartView moveViewToX:models.count - 1];
    }
}

- (void)refreshPriceLine:(KKCryptoChartModel *)model {
    if (!model) {
        return;
    }

    NSUInteger lowX = (NSUInteger)self.mainChartView.lowestVisibleX;
    NSUInteger highX = (NSUInteger)self.mainChartView.highestVisibleX;
    if (self.chartModels.count < highX || lowX > highX) {
        return;
    }
    NSArray<KKCryptoChartModel *> *subArray = [self.chartModels subarrayWithRange:NSMakeRange(lowX, highX - lowX)];
    if (subArray.count == 0) {
        return;
    }
    long long visibleLowestPrice = subArray.firstObject.low.longLongValue, visibleHighestPrice = subArray.firstObject.high.longLongValue;
    for (KKCryptoChartModel *model in subArray) {
        if (visibleLowestPrice > model.low.longLongValue) {
            visibleLowestPrice = model.low.longLongValue;
        }
        if (visibleHighestPrice < model.high.longLongValue) {
            visibleHighestPrice = model.high.longLongValue;
        }
    }
    self.priceLine.limit = model.close.longLongValue;
    UIColor *priceColor = KK_CRYPTO_CHART_PRICE_LINE_MIDDLE_COLOR;
    if (model.close.longLongValue > visibleHighestPrice) {
        priceColor = KK_CRYPTO_CHART_PRICE_LINE_HIGH_COLOR;
    } else if (model.close.longLongValue < visibleLowestPrice) {
        priceColor = KK_CRYPTO_CHART_PRICE_LINE_LOW_COLOR;
    } else {
        priceColor = KK_CRYPTO_CHART_PRICE_LINE_MIDDLE_COLOR;
    }
    self.priceLine.lineColor = priceColor;

    // refresh limit label
    self.priceButton.backgroundColor = priceColor;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:self.config.coinPrecision.integerValue];
    [formatter setMinimumFractionDigits:self.config.coinPrecision.integerValue];
    NSString *priceString = [formatter stringFromNumber:model.close];
    
    CGFloat labelH = self.priceButton.titleLabel.font.lineHeight;
    CGFloat buttonH = labelH + 4.0f;
    CGRect stringRect = [priceString boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, labelH) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : KK_CRYPTO_CHART_YAXIS_TEXT_FONT} context:nil];
    CGFloat buttonW = stringRect.size.width + 4.0f;
    __strong typeof(self) wself = self;
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^{
        if (wself.priceLine.yPx > 0 && wself.priceLine.yPx < CGRectGetHeight(self.mainChartView.frame)) {
            wself.priceButton.hidden = NO;
            wself.priceButton.frame = CGRectMake(wself.mainChartView.viewPortHandler.contentRight - buttonW - 25, wself.priceLine.yPx - buttonH / 2, buttonW, buttonH);
            [wself.priceButton setTitle:priceString forState:UIControlStateNormal];
        } else {
            wself.priceButton.hidden = YES;
        }
    });
    
}

- (void)generateChartData:(NSArray<KKCryptoChartModel *> *)models {
    // generate main data
    self.mainData = [[CombinedChartData alloc] init];
    self.mainData.candleData = [self generateCandleData:models];
    self.mainData.lineData = [self generateLineData:models type:self.currentMainIndType];
    self.mainChartView.data = self.mainData;
    
    // generate volume data
    self.volumeData = [[CombinedChartData alloc] init];
    self.volumeData.barData = [self generateBarData:models type:KKIndicatorVolume];
    self.volumeChartView.data = self.volumeData;

    // generate indicator data
    self.indicatorData = [[CombinedChartData alloc] init];
    self.indicatorData.barData = [self generateBarData:models type:self.currentSideIndType];
    self.indicatorData.lineData = [self generateLineData:models type:self.currentSideIndType];
    if (self.currentSideIndType != KKIndicatorNone) {
        self.indicatorChartView.data = self.indicatorData;
    }
}

// generate line chart data according to indicator model
- (LineChartData *)generateLineData:(NSArray<KKCryptoChartModel *> *)datas type:(KKIndicator)type {
    LineChartData *d = [[LineChartData alloc] init];
    if (type == KKIndicatorNone || type == KKIndicatorTD) {
        return d;
    }
    NSMutableArray *ma1 = [[NSMutableArray alloc] init];
    NSMutableArray *ma2 = [[NSMutableArray alloc] init];
    NSMutableArray *ma3 = [[NSMutableArray alloc] init];

    for (int i = 0; i < datas.count; i++) {
        switch (type) {
            case KKIndicatorMA:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA1 doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA2 doubleValue]]];
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA3 doubleValue]]];
            }
                break;
            case KKIndicatorEMA:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].EMA.EMA1 doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].EMA.EMA2 doubleValue]]];
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].EMA.EMA3 doubleValue]]];
            }
                break;
            case KKIndicatorBOLL:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].BOLL.UP doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].BOLL.MID doubleValue]]];
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].BOLL.LOW doubleValue]]];
            }
                break;
            case KKIndicatorMACD:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MACD.DIFF doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MACD.DEA doubleValue]]];
            }
                break;
            case KKIndicatorKDJ:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].KDJ.K doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].KDJ.D doubleValue]]];
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].KDJ.J doubleValue]]];
            }
                break;
            case KKIndicatorRSI:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].RSI.RSI1 doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].RSI.RSI2 doubleValue]]];
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].RSI.RSI3 doubleValue]]];
            }
                break;
            case KKIndicatorWR:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].WR.WR1 doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].WR.WR2 doubleValue]]];
            }
                break;
            default:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA1 doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA2 doubleValue]]];
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA3 doubleValue]]];
            }
                break;
        }
        
    }
    
    switch (type) {
        case KKIndicatorMA:
        case KKIndicatorEMA:
        case KKIndicatorBOLL:{
            LineChartDataSet *set1 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_1_COLOR entries:ma1 isMainChartLine:YES];
            LineChartDataSet *set2 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_2_COLOR entries:ma2 isMainChartLine:YES];
            LineChartDataSet *set3 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_3_COLOR entries:ma3 isMainChartLine:YES];
            [d addDataSet:set1];
            [d addDataSet:set2];
            [d addDataSet:set3];
        }
            break;
        case KKIndicatorMACD:{
            LineChartDataSet *set1 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_1_COLOR entries:ma1 isMainChartLine:NO];
            LineChartDataSet *set2 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_2_COLOR entries:ma2 isMainChartLine:NO];
            [d addDataSet:set1];
            [d addDataSet:set2];
        }
            break;
        case KKIndicatorKDJ:
        case KKIndicatorRSI:{
            LineChartDataSet *set1 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_1_COLOR entries:ma1 isMainChartLine:NO];
            LineChartDataSet *set2 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_2_COLOR entries:ma2 isMainChartLine:NO];
            LineChartDataSet *set3 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_3_COLOR entries:ma3 isMainChartLine:NO];
            [d addDataSet:set1];
            [d addDataSet:set2];
            [d addDataSet:set3];
        }
            break;
        case KKIndicatorWR:{
            LineChartDataSet *set1 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_1_COLOR entries:ma1 isMainChartLine:NO];
            LineChartDataSet *set2 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_2_COLOR entries:ma2 isMainChartLine:NO];
            [d addDataSet:set1];
            [d addDataSet:set2];
        }
            break;
        default:{
            LineChartDataSet *set1 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_1_COLOR entries:ma1 isMainChartLine:NO];
            LineChartDataSet *set2 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_2_COLOR entries:ma1 isMainChartLine:NO];
            LineChartDataSet *set3 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_3_COLOR entries:ma1 isMainChartLine:YES];
            [d addDataSet:set1];
            [d addDataSet:set2];
            [d addDataSet:set3];
        }
            break;
    }
    
    return d;
}

// common method: setup line chart style
- (LineChartDataSet *)setupLineChartDataSet:(UIColor *)color entries:(NSArray<ChartDataEntry *> *)entries isMainChartLine:(BOOL)isMainChartLine {
    LineChartDataSet *set = [[LineChartDataSet alloc] initWithEntries:entries label:nil];
    [set setColor:color];
    set.lineWidth = 1.0;
    if (isMainChartLine) {
        set.mode = LineChartModeCubicBezier;
    }
    set.drawValuesEnabled = NO;
    set.axisDependency = AxisDependencyRight;
    set.drawCirclesEnabled = NO;
    set.highlightEnabled = NO;
    return set;
}

- (CandleChartData *)generateCandleData:(NSArray<KKCryptoChartModel *> *)datas {
    NSMutableArray *entriesBuy = [[NSMutableArray alloc] init];
    NSMutableArray *entriesSell = [[NSMutableArray alloc] init];
    NSMutableArray *entriesBuyNinth = [[NSMutableArray alloc] init];
    NSMutableArray *entriesSellNinth = [[NSMutableArray alloc] init];
    NSMutableArray *entriesBuyThirteenth = [[NSMutableArray alloc] init];
    NSMutableArray *entriesSellThirteenth = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < datas.count; i++) {
        KKCryptoChartModel *data = datas[i];
        double high = [data.high doubleValue];
        double low = [data.low doubleValue];
        double open = [data.open doubleValue];
        double close = [data.close doubleValue];
        if (data.TD && data.TD.buySetupIndex > 0) {
            if (data.TD.buySetupIndex.integerValue == 9) {
                UIImage *image = [UIImage imageNamed:@"td_buy_ninth" inBundle:[BundleUtil getBundle] compatibleWithTraitCollection:nil];
                [entriesBuyNinth addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:image data:data.TD precision:self.config.coinPrecision.integerValue]];
            } else if (data.TD.buySetupIndex.integerValue == 13) {
                [entriesBuyThirteenth addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:nil data:data.TD precision:self.config.coinPrecision.integerValue]];
            } else {
                [entriesBuy addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:nil data:data.TD precision:self.config.coinPrecision.integerValue]];
            }
            
        } else {
            if (data.TD.sellSetupIndex.integerValue == 9) {
                UIImage *image = [UIImage imageNamed:@"td_sell_ninth" inBundle:[BundleUtil getBundle] compatibleWithTraitCollection:nil];
                [entriesSellNinth addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:image data:data.TD precision:self.config.coinPrecision.integerValue]];
            } else if (data.TD.sellSetupIndex.integerValue == 13) {
                [entriesSellThirteenth addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:nil data:data.TD precision:self.config.coinPrecision.integerValue]];
            } else {
                [entriesSell addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:nil data:data.TD precision:self.config.coinPrecision.integerValue]];
            }
            
        }
        
    }
    CandleChartData *d = [[CandleChartData alloc] initWithDataSets:@[[self setUpCandleDataSet:entriesBuy tdIndicatorType:KKTDIndicatorTypeBuy], [self setUpCandleDataSet:entriesBuyNinth tdIndicatorType:KKTDIndicatorTypeBuyNinth], [self setUpCandleDataSet:entriesBuyThirteenth tdIndicatorType:KKTDIndicatorTypeBuyThirteenth], [self setUpCandleDataSet:entriesSell tdIndicatorType:KKTDIndicatorTypeSell], [self setUpCandleDataSet:entriesSellNinth tdIndicatorType:KKTDIndicatorTypeSellNinth], [self setUpCandleDataSet:entriesSellThirteenth tdIndicatorType:KKTDIndicatorTypeSellThirteenth]]];
    return d;
}

- (CandleChartDataSet *)setUpCandleDataSet:(NSArray<CandleChartDataEntry *> *)entries tdIndicatorType:(KKTDIndicatorType)type {
    CandleChartDataSet *candleSet = [[CandleChartDataSet alloc] initWithEntries:entries label:nil];
    candleSet.valueFormatter = [[TDIndicatorValueFormatter alloc] init];
    candleSet.valueFont = [UIFont systemFontOfSize:9.f];
    switch (type) {
        case KKTDIndicatorTypeBuy:
            candleSet.valueTextColor = KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR;
            break;
        case KKTDIndicatorTypeSell:
            candleSet.valueTextColor = KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR;
            break;
        default:
            break;
    }
    
    if (self.currentMainIndType == KKIndicatorTD) {
        if (type == KKTDIndicatorTypeBuy || type == KKTDIndicatorTypeSell) {
            candleSet.drawIconsEnabled = NO;
            candleSet.drawValuesEnabled = YES;
        } else {
            candleSet.drawIconsEnabled = YES;
            candleSet.drawValuesEnabled = NO;
            candleSet.iconsOffset = CGPointMake(0, -15);
        }
    } else {
        candleSet.drawIconsEnabled = NO;
        candleSet.drawValuesEnabled = NO;
        candleSet.iconsOffset = CGPointMake(0, -15);
    }
    
//    candleSet.valueTextColor = isBuySetup ? KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR : KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR;

    // color
    candleSet.shadowColorSameAsCandle = YES;
    candleSet.shadowWidth = KK_CRYPTO_CHART_CANDLE_SHADOW_LINE_WIDTH;

    // fix charts framework color error
    candleSet.decreasingColor = KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR;
    candleSet.decreasingFilled = YES;
    candleSet.increasingColor = KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR;
    candleSet.increasingFilled = YES;
    candleSet.neutralColor = KK_CRYPTO_CHART_CANDLE_NEUTRAL_COLOR;
    
    // axis
    candleSet.axisDependency = AxisDependencyRight;
    
    // highlight
    candleSet.highlightEnabled = YES;
    
    return candleSet;
}

// KKIndicatorNone:Volume data; KKIndicatorMACD: MACD bar data
- (BarChartData *)generateBarData:(NSArray<KKCryptoChartModel *> *)datas type:(KKIndicator)type {
    BarChartData *d = [[BarChartData alloc] init];
    if ((type != KKIndicatorVolume) && (type != KKIndicatorMACD)) {
        return d;
    }
    NSMutableArray<BarChartDataEntry *> *entries1 = [[NSMutableArray alloc] init];
    NSMutableArray<BarChartDataEntry *> *entries2 = [[NSMutableArray alloc] init];
    for (int index = 0; index < datas.count; index++) {
        if ([datas[index].open doubleValue] < [datas[index].close doubleValue]) {
            switch (type) {
                case KKIndicatorVolume:{
                    [entries1 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].volume doubleValue]]];
                }
                    break;
                case KKIndicatorMACD:{
                    [entries1 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].MACD.MACD doubleValue]]];
                }
                    break;
                default:{
                    [entries1 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].volume doubleValue]]];
                }
                    break;
            }
            
        } else {
            switch (type) {
                case KKIndicatorVolume:{
                    [entries2 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].volume doubleValue]]];
                }
                    break;
                case KKIndicatorMACD:{
                    [entries2 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].MACD.MACD doubleValue]]];
                }
                    break;
                default:{
                    [entries2 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].volume doubleValue]]];
                }
                    break;
            }
            
        }
    }

    // setup increase style, fix charts framework color error

    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithEntries:entries1 label:nil];
    [set1 setColor:KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR];
    set1.drawValuesEnabled = NO;
    set1.axisDependency = AxisDependencyRight;
    
    // setup decrese style
    BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithEntries:entries2 label:nil];
    [set2 setColor:KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR];
    set2.drawValuesEnabled = NO;
    set2.axisDependency = AxisDependencyRight;

    d = [[BarChartData alloc] initWithDataSets:@[set1, set2]];
    
    return d;
}

- (void)adjustAxis:(NSArray<KKCryptoChartModel *> *)models {
    self.mainChartView.xAxis.axisMinimum = self.mainData.xMin - KKCryptoChartXAxisOffset;
    self.mainChartView.xAxis.axisMaximum = self.mainData.xMax + KKCryptoChartXAxisOffset;
    self.volumeChartView.xAxis.axisMinimum = self.volumeChartView.data.xMin - KKCryptoChartXAxisOffset;
    self.volumeChartView.xAxis.axisMaximum = self.volumeChartView.data.xMax + KKCryptoChartXAxisOffset;
    self.indicatorChartView.xAxis.axisMinimum = self.indicatorChartView.data.xMin - KKCryptoChartXAxisOffset;
    self.indicatorChartView.xAxis.axisMaximum = self.indicatorChartView.data.xMax + KKCryptoChartXAxisOffset;
    NSInteger b = [self.config.coinPrecision integerValue];


    self.mainChartViewRightAxis.valueFormatter = [[PriceValueFormatter alloc] initWithPrecision:b];


    
    if (self.isShowIndicatorChartView) {
        self.volumeChartView.xAxis.drawLabelsEnabled = NO;
        self.volumeChartView.xAxis.drawAxisLineEnabled = NO;
        self.indicatorChartView.xAxis.drawLabelsEnabled = YES;
        self.indicatorChartView.xAxis.drawAxisLineEnabled = YES;
//        if (models.count <= 10) {
//            [self.indicatorChartView.xAxis setLabelCount:1 force:YES];
//        } else {
//            [self.indicatorChartView.xAxis setLabelCount: XAXIS_LABEL_COUNT force:YES];
//        }
        [self.indicatorChartView.xAxis setValueFormatter:[self setUpXAxisValueFormatter:models]];
    } else {
        self.volumeChartView.xAxis.drawLabelsEnabled = YES;
        self.volumeChartView.xAxis.drawAxisLineEnabled = YES;
        if (models.count <= 10) {
            [self.volumeChartView.xAxis setLabelCount:1 force:YES];
        } else {
            [self.volumeChartView.xAxis setLabelCount: XAXIS_LABEL_COUNT force:YES];
        }
        self.indicatorChartView.xAxis.drawLabelsEnabled = NO;
        self.indicatorChartView.xAxis.drawAxisLineEnabled = NO;
        [self.volumeChartView.xAxis setValueFormatter:[self setUpXAxisValueFormatter:models]];
    }

    if (self.currentSideIndType == KKIndicatorMACD) {
        [self.indicatorChartView.rightAxis resetCustomAxisMin];
    } else {
        self.indicatorChartView.rightAxis.axisMinimum = 0.0f;
    }
}

- (ChartDefaultAxisValueFormatter *)setUpXAxisValueFormatter:(NSArray<KKCryptoChartModel *> *)models {
    ChartDefaultAxisValueFormatter *formatter = [[ChartDefaultAxisValueFormatter alloc] initWithBlock:^NSString * _Nonnull(double value, ChartAxisBase * _Nullable axis) {
        // fix x axis overlap
        if (models.count <= 2) {
            value = value + KKCryptoChartXAxisOffset;
        }
        if ((NSInteger)value >= models.count) {
            KKLog(@"setUpXAxisValueFormatter value=%f, models count=%lu", value, (unsigned long)models.count);
            return @"";
        }
        NSNumber *timestamp = models[(NSInteger)value].timestamp;
        return [self getFormatterStringFromTimeType:models[(NSInteger)value].timeType timestamp:timestamp];
    }];
    return formatter;
}

- (NSString *)getFormatterStringFromTimeType:(NSString *)timeType timestamp:(NSNumber *)timestamp {
    KKCryptoChartTimeType type = [self.dataManager getTimeTypeEnum:timeType];
    NSString *formatteredValue = nil;
    switch (type) {
        case KKCryptoChartTimeTypeOneMinute:
        case KKCryptoChartTimeTypeFiveMinutes:
        case KKCryptoChartTimeTypeFifteenMinutes:
        case KKCryptoChartTimeTypeThirtyMinutes:
        case KKCryptoChartTimeTypeOneHour:
            formatteredValue = [DateUtil getHourMinuteFromTimestamp:timestamp.stringValue];
            break;
        case KKCryptoChartTimeTypeOneDay:
        case KKCryptoChartTimeTypeOneWeek:
        case KKCryptoChartTimeTypeOneMonth:
            formatteredValue = [DateUtil getMonthDayFromTimestamp:timestamp.stringValue];
            break;
        case KKCryptoChartTimeTypeOneYear:
            formatteredValue = [DateUtil getYearFromTimestamp:timestamp.stringValue];
            break;
        default:
            formatteredValue = [DateUtil getHourMinuteFromTimestamp:timestamp.stringValue];
            break;
    }
    return formatteredValue;
}

- (CGFloat)calMaxScale:(CGFloat)count {
    return count / CANDLE_CHART_CANDLE_COUNT;
}

#pragma mark - network
- (void)fetchCloudData:(KKCryptoChartGlobalConfig*)config{
    self.dataManager.config = config;
    [self setUpSelectorData];
    __strong typeof(self) weakSelf = self;
    [self.loadingView startAnimating];
    [self.dataManager fetchCloudDataWithCoinCode:config.coinCode timeType:config.timeType environment:config.environment completionHandler:^(NSArray<KKCryptoChartModel *> * _Nonnull chartModels, NSString *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.loadingView stopAnimating];
            if (chartModels) {
                weakSelf.chartModels = chartModels;
                [weakSelf setUpChartModels:chartModels config:config];
            }
            if (error) {
                if (weakSelf.bugReportDelegate && [weakSelf.bugReportDelegate respondsToSelector:@selector(didErrorOccurred:error:)]) {
                    [weakSelf.bugReportDelegate didErrorOccurred:weakSelf error:error];
                }
            }
        });
    }];
}

- (void)setupWebSocketData:(KKCryptoChartGlobalConfig *)config {
    NSString *url;
    if (config.environment == KKCryptoChartEnvironmentTypeProduct) {
        url = KK_WEB_SOCKET_PROD_URL;
    } else {
        url = KK_WEB_SOCKET_BETA_URL;
    }
    KKCryptoChartDataManager *manager = [KKCryptoChartDataManager shareManager];
    NSString *resolution = [manager getSocketResolution:[manager getTimeTypeEnum:config.timeType]];
    if (self.webSocket) {
        [self.webSocket updateSocket:config.coinCode fromSymbol:self.fromSymbol resolution:resolution];
    } else {
        self.webSocket = [[KKWebSocket alloc] initWithURLString:url symbol:config.coinCode fromSymbol:self.fromSymbol resolution:resolution];
        [self.webSocket open];
    }
    self.webSocket.delegate = self;
    self.fromSymbol = config.coinCode;
}

#pragma mark - ChartViewDelegate

- (void)chartScaled:(ChartViewBase *)chartView scaleX:(CGFloat)scaleX scaleY:(CGFloat)scaleY {
    KKLog(@"chartScaled triggered, chartView:%@", chartView.description);
    [self hideHighlightLine];
    [self showCrossLine:NO];
    [self.chartDescription updateDescription:self.chartModels.lastObject];
    if (chartView == self.mainChartView) {
        CGAffineTransform currentMatrix =  chartView.viewPortHandler.touchMatrix;
        [self.volumeChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.volumeChartView invalidate:YES];
        [self.indicatorChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.indicatorChartView invalidate:YES];
    } else if (chartView == self.volumeChartView) {
        CGAffineTransform currentMatrix =  chartView.viewPortHandler.touchMatrix;
        [self.mainChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.mainChartView invalidate:YES];
        [self.indicatorChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.indicatorChartView invalidate:YES];
    } else {
        CGAffineTransform currentMatrix =  chartView.viewPortHandler.touchMatrix;
        [self.mainChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.mainChartView invalidate:YES];
        [self.volumeChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.volumeChartView invalidate:YES];
    }
    
    // refresh price line
    [self refreshPriceLine:self.chartModels.lastObject];
}

- (void)chartTranslated:(ChartViewBase *)chartView dX:(CGFloat)dX dY:(CGFloat)dY {
    KKLog(@"chartTranslated triggered, chartView:%@", chartView.description);
    [self hideHighlightLine];
    [self showCrossLine:NO];
    [self.chartDescription updateDescription:self.chartModels.lastObject];
    
    // update highest & lowest
    
//    KKCryptoChartModel *highestModel = self.chartModels[(NSInteger)self.mainChartView.highestVisibleX];
//    KKLog(@"chartTranslated highestModel:%@", highestModel.high);
//    KKCryptoChartModel *lowestModel = self.chartModels[(NSInteger)self.mainChartView.lowestVisibleX];
//    KKLog(@"chartTranslated lowestModel:%@", lowestModel.low);
    // sync multi-chart
    if (chartView == self.mainChartView) {
        CGAffineTransform currentMatrix =  chartView.viewPortHandler.touchMatrix;
        [self.volumeChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.volumeChartView invalidate:YES];
        [self.indicatorChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.indicatorChartView invalidate:YES];
    } else if (chartView == self.volumeChartView) {
        CGAffineTransform currentMatrix =  chartView.viewPortHandler.touchMatrix;
        [self.mainChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.mainChartView invalidate:YES];
        [self.indicatorChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.indicatorChartView invalidate:YES];
    } else {
        CGAffineTransform currentMatrix =  chartView.viewPortHandler.touchMatrix;
        [self.mainChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.mainChartView invalidate:YES];
        [self.volumeChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.volumeChartView invalidate:YES];
    }
    
    // refresh price line
    [self refreshPriceLine:self.chartModels.lastObject];
}

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight {
    KKLog(@"chartValueSelected, entry:%@", entry.description);
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView {
    KKLog(@"chartValueNothingSelected");
}
//停止移动时调用
- (void)chartViewDidEndPanning:(ChartViewBase *)chartView{
    KKLog(@"chartViewDidEndPanning");
    [self showCrossLine:NO];
}

#pragma mark - KKCryptoChartTabBarDelegate

- (void)didTabItemSelected:(NSInteger)index title:(NSString *)title {
    KKLog(@"didTabItemSelected, index=%ld, title=%@", (long)index, title);
    if (index == KKCryptoChartTabBarMore) {
        self.indicatorSelector.hidden = YES;
        if (!self.moreSelector) {
            self.moreSelector = [[KKCryptoMoreSelector alloc] init];
            [self.moreSelector setFrame:CGRectMake(0, CGRectGetMaxY(self.chartTabBar.frame) + 12, self.frame.size.width, 30)];
            [self.moreSelector setUpTypes:self.moreSelectorTypes titles:self.moreSelectorTitles];
            self.moreSelector.delegate = self;
            [self addSubview:self.moreSelector];
            self.moreSelector.hidden = YES;
        }
        self.moreSelector.hidden = !self.moreSelector.hidden;
    } else if (index == KKCryptoChartTabBarIndicator) {
        self.moreSelector.hidden = YES;
        if (!self.indicatorSelector) {
            self.indicatorSelector = [[KKIndicatorSelector alloc] init];
            [self.indicatorSelector setFrame:CGRectMake(0, CGRectGetMaxY(self.chartTabBar.frame) + 12, self.frame.size.width, 60)];
            self.indicatorSelector.delegate = self;
            [self addSubview:self.indicatorSelector];
            [self.indicatorSelector setUpData];
            self.indicatorSelector.hidden = YES;
        }
        self.indicatorSelector.hidden = !self.indicatorSelector.hidden;
    } else {
        self.moreSelector.hidden = YES;
        self.indicatorSelector.hidden = YES;
        [self.moreSelector setAllBtnUnselected];
        NSString *timeType = [self.dataManager getTimeString:index];
        if (timeType) {
            self.config.timeType = timeType;
            [self updateConfig:self.config];
        }
    }
    
    if (self.infoLayer.hidden == NO) {
        [self showCrossLine:NO];
    }
}

#pragma mark - KKIndicatorSelectorDelegate

- (void)indicatorSelectorMainChartValueChanged:(KKIndicator)currentIndicator {
    self.currentMainIndType = currentIndicator;
    if (!self.chartModels) {
        return;
    }
    switch (currentIndicator) {
        case KKIndicatorTD: {
            self.mainData.lineData = [self generateLineData:self.chartModels type:currentIndicator];
            [self.mainData.candleData.dataSets enumerateObjectsUsingBlock:^(id<IChartDataSet>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (CGColorEqualToColor(obj.valueTextColor.CGColor, KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR.CGColor)) {
                    obj.drawValuesEnabled = YES;
                    obj.drawIconsEnabled = NO;
                } else if (CGColorEqualToColor(obj.valueTextColor.CGColor, KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR.CGColor)) {
                    obj.drawValuesEnabled = YES;
                    obj.drawIconsEnabled = NO;
                } else {
                    obj.drawValuesEnabled = NO;
                    obj.drawIconsEnabled = YES;
                }
                
            }];
            self.mainChartView.data = self.mainData;
            [self.mainChartView.data notifyDataChanged];
            [self.mainChartView notifyDataSetChanged];
        }
            break;
        case KKIndicatorMA:
        case KKIndicatorEMA:
        case KKIndicatorBOLL:
        case KKIndicatorNone:
        default:{
            self.mainData.lineData = [self generateLineData:self.chartModels type:currentIndicator];
            [self.mainData.candleData.dataSets enumerateObjectsUsingBlock:^(id<IChartDataSet>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.drawValuesEnabled = NO;
            }];
            self.mainChartView.data = self.mainData;
            [self.mainChartView.data notifyDataChanged];
            [self.mainChartView notifyDataSetChanged];
        }
            break;
    }
    [self.mainIndicatorDesc setupDescription:self.chartModels.lastObject type:currentIndicator config:self.config];
}

- (void)indicatorSelectorSideChartValueChanged:(KKIndicator)currentIndicator {
    self.currentSideIndType = currentIndicator;
    [self changeChartViewUI:currentIndicator];
    switch (currentIndicator) {
        case KKIndicatorKDJ:
        case KKIndicatorRSI:
        case KKIndicatorWR:{
            self.indicatorData.lineData = [self generateLineData:self.chartModels type:currentIndicator];
            self.indicatorData.barData = [[BarChartData alloc] init];
            self.indicatorChartView.data = self.indicatorData;
            [self adjustAxis:self.chartModels];
            [self.indicatorChartView.data notifyDataChanged];
            [self.indicatorChartView notifyDataSetChanged];
        }
            break;
        case KKIndicatorMACD:{
            self.indicatorData.lineData = [self generateLineData:self.chartModels type:currentIndicator];
            self.indicatorData.barData = [self generateBarData:self.chartModels type:currentIndicator];
            self.indicatorChartView.data = self.indicatorData;
            [self adjustAxis:self.chartModels];
            [self.indicatorChartView.data notifyDataChanged];
            [self.indicatorChartView notifyDataSetChanged];
        }
            break;
        case KKIndicatorNone:
        default:{
            [self adjustAxis:self.chartModels];
            [self.indicatorChartView clear];
        }
            break;
    }
    [self.sideIndicatorDesc setupDescription:self.chartModels.lastObject type:currentIndicator config:self.config];
}

- (void)changeChartViewUI:(KKIndicator)currentIndicator {
    switch (currentIndicator) {
        case KKIndicatorMACD:
        case KKIndicatorKDJ:
        case KKIndicatorRSI:
        case KKIndicatorWR:
            KKLog(@"changeChartViewUI volume view height = %f", self.chartViewHeight * VOLUME_VIEW_RATIO * 0.5f);
            self.isShowIndicatorChartView = YES;
            [self setNeedsLayout];
            break;
        case KKIndicatorNone:
            self.isShowIndicatorChartView = NO;
            [self setNeedsLayout];
            break;
        default:
            break;
    }
}

#pragma mark - KKMoreSelectorDelegate

- (void)didMoreSelectorSelected:(NSInteger)type title:(NSString *)title {
    KKLog(@"didMoreSelectorSelected type=%ld, title=%@", (long)type, title);
    self.moreSelector.hidden = YES;
    [self.chartTabBar updateMoreTabItem:KKCryptoChartTabBarMore title:title];
    self.config.timeType = [self.dataManager getTimeString:type];
    [self updateConfig:self.config];
    //如果moreview点击了,把tabbar的more按钮设置为选中
    [[TabbarSelectUtil share] setTabbarSelectBtnTagWithTag:KKCryptoChartTabBarMore];
    
}

#pragma mark - KKWebSocketDelegate

- (void)didReceiveKlineData:(NSDictionary *)klineData {
    if (!klineData) {
        return;
    }
    if (!self.chartModels || self.chartModels.count == 0) {
        return;
    }
    KKCryptoChartModel *model = [self setupWebSocketModel:klineData];
    if (!model.timestamp) {
        return;
    }
    NSMutableArray *mutArray = [[NSMutableArray alloc] initWithArray:self.chartModels];
    if ([self checkSameTime:model.timestamp.stringValue toTimestamp:self.chartModels.lastObject.timestamp.stringValue]) {
        [mutArray replaceObjectAtIndex:mutArray.count - 1 withObject:model];
    } else {
        [mutArray addObject:model];
    }
    self.chartModels = [NSArray arrayWithArray:mutArray];
    
    [[KKCryptoChartDataManager shareManager] calculateIndicatorData:self.chartModels];
    
    [self generateChartData:self.chartModels];

    [self setupDescription:self.chartModels.lastObject config:self.config];
    
    [self refreshPriceLine:model];

//    ChartDataEntry *entry = nil;
//    for (CandleChartDataSet *dataSet in self.mainChartView.candleData.dataSets) {
//        if (!entry) {
//            entry = [dataSet entryForIndex:dataSet.entryCount - 1];
//        }
//        if ([dataSet entryForIndex:dataSet.entryCount - 1].x > entry.x) {
//            entry = [dataSet entryForIndex:dataSet.entryCount - 1];
//        }
//    }
    
    [self.mainChartView.data notifyDataChanged];
    [self.mainChartView notifyDataSetChanged];

    [self.volumeChartView.data notifyDataChanged];
    [self.volumeChartView notifyDataSetChanged];

    if (self.isShowIndicatorChartView) {
        [self.indicatorChartView.data notifyDataChanged];
        [self.indicatorChartView notifyDataSetChanged];
    }
}

- (BOOL)checkSameTime:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp {
    BOOL isSameTime = NO;
    KKCryptoChartDataManager *manager = [KKCryptoChartDataManager shareManager];
    KKCryptoChartTimeType timeType = [manager getTimeTypeEnum:self.config.timeType];
    if (timeType == KKCryptoChartTimeTypeOneMinute) {
        isSameTime = [DateUtil isSameMinuteInterval:fromTimestamp toTimestamp:toTimestamp interval:1];
    } else if (timeType == KKCryptoChartTimeTypeFiveMinutes) {
        isSameTime = [DateUtil isSameMinuteInterval:fromTimestamp toTimestamp:toTimestamp interval:5];
    } else if (timeType == KKCryptoChartTimeTypeFifteenMinutes) {
        isSameTime = [DateUtil isSameMinuteInterval:fromTimestamp toTimestamp:toTimestamp interval:15];
    } else if (timeType == KKCryptoChartTimeTypeThirtyMinutes) {
        isSameTime = [DateUtil isSameMinuteInterval:fromTimestamp toTimestamp:toTimestamp interval:30];
    } else if (timeType == KKCryptoChartTimeTypeOneHour) {
        isSameTime = [DateUtil isSameHourInterval:fromTimestamp toTimestamp:toTimestamp interval:1];
    } else if (timeType == KKCryptoChartTimeTypeFourHours) {
        isSameTime = [DateUtil isSameHourInterval:fromTimestamp toTimestamp:toTimestamp interval:4];
    } else if (timeType == KKCryptoChartTimeTypeOneDay) {
        isSameTime = [DateUtil isSameDay:fromTimestamp toTimestamp:toTimestamp];
    } else if (timeType == KKCryptoChartTimeTypeOneWeek) {
        isSameTime = [DateUtil isSameWeek:fromTimestamp toTimestamp:toTimestamp];
    } else if (timeType == KKCryptoChartTimeTypeOneMonth) {
        isSameTime = [DateUtil isSameMonth:fromTimestamp toTimestamp:toTimestamp];
    } else if (timeType == KKCryptoChartTimeTypeOneYear) {
        isSameTime = [DateUtil isSameYear:fromTimestamp toTimestamp:toTimestamp];
    }
    return isSameTime;
}

- (KKCryptoChartModel *)setupWebSocketModel:(NSDictionary *)klineData {
    KKCryptoChartModel *model = [[KKCryptoChartModel alloc] init];
    model.coinCode = [klineData valueForKey:@"symbol"];
    model.open = [klineData valueForKey:@"open"];
    model.close = [klineData valueForKey:@"close"];
    model.high = [klineData valueForKey:@"high"];
    model.low = [klineData valueForKey:@"low"];
    model.timestamp = [klineData valueForKey:@"closeTime"];
    model.timeType = [[KKCryptoChartDataManager shareManager] getTimeStringFromSocketResolution:[klineData valueForKey:@"timeFrame"]];
    model.volume = [klineData valueForKey:@"volume"];
    model.change = [klineData valueForKey:@"change"];
    model.percentage = [klineData valueForKey:@"percentage"];
    return model;
}

#pragma mark - Tap Action

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint touchPoint = [touch locationInView:self];
    [self updateSelector:touchPoint];
    return YES;
}

- (void)updateSelector:(CGPoint)location {
    CGPoint moreLcation = [self.moreSelector.layer convertPoint:location fromLayer:self.layer];
    CGPoint tabBarLocation = [self.chartTabBar.layer convertPoint:location fromLayer:self.layer];
    CGPoint indicatorLocation = [self.indicatorSelector.layer convertPoint:location fromLayer:self.layer];

    // close selector

    if (!self.moreSelector.hidden) {
        if (![self.moreSelector.layer containsPoint:moreLcation] && ![self.chartTabBar.layer containsPoint:tabBarLocation]) {
            self.moreSelector.hidden = YES;
            [self.chartTabBar updateSelectorTabStatus];
        }
    }

    if (!self.indicatorSelector.hidden) {
        if (![self.indicatorSelector.layer containsPoint:indicatorLocation] && ![self.chartTabBar.layer containsPoint:tabBarLocation]) {
            self.indicatorSelector.hidden = YES;
            [self.chartTabBar updateSelectorTabStatus];
        }
    }
}

- (void)updateInfoLayer:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint location;
    CGPoint chartViewLocation = [gestureRecognizer locationInView:self];
    CGPoint mainChartLcation = [gestureRecognizer locationInView:self.mainChartView];
    CGPoint volumeChartLcation = [gestureRecognizer locationInView:self.volumeChartView];
    CGPoint indicatorChartLcation = [gestureRecognizer locationInView:self.indicatorChartView];
//    CGPoint mainChartLcation = [self.mainChartView.layer convertPoint:location fromLayer:self.layer];
//    CGPoint volumeChartLcation = [self.volumeChartView.layer convertPoint:location fromLayer:self.layer];

    CombinedChartView *tapChartView;
    if ([self.mainChartView.layer containsPoint:mainChartLcation]) {
        tapChartView = self.mainChartView;
        location = mainChartLcation;
    } else if ([self.volumeChartView.layer containsPoint:volumeChartLcation]) {
        tapChartView = self.volumeChartView;
        location = volumeChartLcation;
    } else {
        tapChartView = self.indicatorChartView;
        location = indicatorChartLcation;
    }

    ChartDataEntry *entry = [tapChartView getEntryByTouchPointWithPoint:location];
    if (self.chartModels.count <= (NSInteger)entry.x) {
        return;
    }
    KKCryptoChartModel *model = self.chartModels[(NSInteger)entry.x];
    ChartHighlight *highlight = [tapChartView getHighlightByTouchPoint:location];
    if (highlight != nil) {
        [self drawCrossLine:CGPointMake(highlight.xPx, chartViewLocation.y)];
        [self drawInfoLayerText:CGPointMake(highlight.xPx, chartViewLocation.y) model:model];
        [self showCrossLine:YES];
        
        // 延迟隐藏
//        [self performSelector:@selector(hideCrossLine) withObject:nil afterDelay:3.0f];
    }

    [self.infoLayer updateAlertValueWithModel:model];
}

- (void)drawInfoLayerText:(CGPoint)location model:(KKCryptoChartModel *)model {
    [self drawVolumeText:location model:model];
    [self drawPriceText:location model:model];
}

- (void)drawVolumeText:(CGPoint)location model:(KKCryptoChartModel *)model {
    // 时间
    NSString *volumeString = [self getFormatterStringFromTimeType:model.timeType timestamp:model.timestamp];
    if (!volumeString) {
        return;
    }

    CGSize volumeTextSize = [volumeString sizeWithAttributes:@{ @"NSFontAttributeName" : KK_CRYPTO_CHART_XAXIS_TEXT_FONT }];
    CGRect volumeRect = CGRectMake(location.x - (volumeTextSize.width + AXIS_INFO_LAYER_X_OFFSET) / 2, CGRectGetMaxY(self.volumeChartView.frame) - KK_CRYPTO_CHART_XAXIS_TEXT_FONT.lineHeight, volumeTextSize.width + AXIS_INFO_LAYER_X_OFFSET * 2, KK_CRYPTO_CHART_XAXIS_TEXT_FONT.lineHeight);
    self.volumeLayer.fontSize = 10;
    self.volumeLayer.text = volumeString;
    self.volumeLayer.frame = volumeRect;
    self.volumeLayer.alignmentMode = kCAAlignmentCenter;
    self.volumeLayer.xOffset = AXIS_INFO_LAYER_X_OFFSET;
    [self.layer insertSublayer:self.volumeLayer atIndex:100];
}

- (void)drawPriceText:(CGPoint)location model:(KKCryptoChartModel *)model {
    // 价格
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:self.config.coinPrecision.integerValue];
    [formatter setMinimumFractionDigits:self.config.coinPrecision.integerValue];
    NSString *priceString = [formatter stringFromNumber:model.close];
    if (!priceString) {
        return;
    }

    CGSize priceTextSize = [priceString sizeWithAttributes:@{ @"NSFontAttributeName" : KK_CRYPTO_CHART_YAXIS_TEXT_FONT }];
    CGRect priceRect = CGRectMake(CGRectGetMaxX(self.mainChartView.frame) - priceTextSize.width - AXIS_INFO_LAYER_X_OFFSET * 2 - 5.0, location.y - KK_CRYPTO_CHART_YAXIS_TEXT_FONT.lineHeight / 2, priceTextSize.width + AXIS_INFO_LAYER_X_OFFSET * 2, KK_CRYPTO_CHART_YAXIS_TEXT_FONT.lineHeight);
    self.priceLayer.fontSize = 8;
    self.priceLayer.text = priceString;
    self.priceLayer.frame = priceRect;
    self.priceLayer.alignmentMode = kCAAlignmentCenter;
    self.priceLayer.xOffset = AXIS_INFO_LAYER_X_OFFSET;
    [self.layer insertSublayer:self.priceLayer atIndex:100];
}

- (void)priceButtonClicked:(UIButton *)button {
    [self chartMoveToRight:self.chartModels isAnimated:YES];
}


#pragma mark - Long Press Actions

- (void)longPress:(UILongPressGestureRecognizer *)longPress {
    CGPoint location = [longPress locationInView:self];
    CGPoint mainLocation = [_mainChartView.layer convertPoint:location fromLayer:self.layer];
    CGPoint volumeLocation = [_volumeChartView.layer convertPoint:location fromLayer:self.layer];
    CGPoint indicatorLocation = [_indicatorChartView.layer convertPoint:location fromLayer:self.layer];
    if ([_mainChartView.layer containsPoint:mainLocation]) {
        KKLog(@"longPress in main chart view");
        ChartHighlight *highlight = [_mainChartView getHighlightByTouchPoint:location];
        if (highlight != nil) {
            [self drawHighlightLine:CGPointMake(highlight.xPx, highlight.yPx)];
        }
        ChartDataEntry *entry = [_mainChartView getEntryByTouchPointWithPoint:location];
        KKCryptoChartModel *model = self.chartModels[(NSInteger)entry.x];
        [self.chartDescription updateDescription:model];
    } else if ([_volumeChartView.layer containsPoint:volumeLocation]) {
        KKLog(@"longPress in volume chart view");
        ChartHighlight *highlight = [_volumeChartView getHighlightByTouchPoint:location];
        if (highlight != nil) {
            CGPoint transPoint = [self convertPoint:CGPointMake(highlight.xPx, highlight.yPx) fromView:_volumeChartView];
            [self drawHighlightLine:transPoint];
        }
        ChartDataEntry *entry = [_mainChartView getEntryByTouchPointWithPoint:location];
        KKCryptoChartModel *model = self.chartModels[(NSInteger)entry.x];
        [self.chartDescription updateDescription:model];
    } else if ([_volumeChartView.layer containsPoint:indicatorLocation]) {
        KKLog(@"longPress in indicator chart view");
        ChartHighlight *highlight = [_indicatorChartView getHighlightByTouchPoint:location];
        if (highlight != nil) {
            CGPoint transPoint = [self convertPoint:CGPointMake(highlight.xPx, highlight.yPx) fromView:_indicatorChartView];
            [self drawHighlightLine:transPoint];
        }
        ChartDataEntry *entry = [_indicatorChartView getEntryByTouchPointWithPoint:location];
        KKCryptoChartModel *model = self.chartModels[(NSInteger)entry.x];
        [self.chartDescription updateDescription:model];
    } else {
        KKLog(@"longPress in other chart view");
    }
}

- (void)drawHighlightLine:(CGPoint)point {
    if (!self.verticalLine) {
        self.verticalLine = [[UIView alloc] init];
        [self addSubview:self.verticalLine];
    }
    if (!self.horizontalLine) {
        self.horizontalLine = [[UIView alloc] init];
        [self addSubview:self.horizontalLine];
    }
    self.verticalLine.hidden = NO;
    self.horizontalLine.hidden = NO;
    [self.verticalLine setFrame:CGRectMake(point.x, 0, 1.0, self.frame.size.height - 20)];
    [self.horizontalLine setFrame:CGRectMake(0, point.y, self.frame.size.width - 40, 1.0)];
    self.verticalLine.backgroundColor = [UIColor colorWithRed:66/255.f green:66/255.f blue:66/255.f alpha:0.8f];
    self.horizontalLine.backgroundColor = [UIColor colorWithRed:66/255.f green:66/255.f blue:66/255.f alpha:0.8f];
}

- (void)hideHighlightLine {
    if (self.verticalLine) {
        self.verticalLine.hidden = YES;
    }
    if (self.horizontalLine) {
        self.horizontalLine.hidden = YES;
    }
}

//绘制十字交叉线
- (void)drawCrossLine:(CGPoint)point {
    
    if (self.loadingView.animating){
        return;
    }
    
    CGFloat y = point.y;
    CGFloat x = point.x;
    
    //横线
    UIBezierPath *path_h = [[UIBezierPath alloc]init];
    [path_h moveToPoint:CGPointMake(0, y)];
    [path_h addLineToPoint:CGPointMake(self.mainChartView.frame.size.width, y)];
    self.horizontalLineLayer.path = path_h.CGPath;
    [self.horizontalLineLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:2], nil]];
    [self.horizontalLineLayer setStrokeColor:CROSS_SHAPED_CIRCLE_POINT_COLOR.CGColor];
    [self.layer insertSublayer:self.horizontalLineLayer atIndex:10];
    
    //竖线
    UIBezierPath *path_x = [[UIBezierPath alloc]init];
    [path_x moveToPoint:CGPointMake(x, self.chartTabBar.frame.size.height)];
    CGFloat lineHeight = self.frame.size.height - [[self.volumeChartView.xAxis getLongestLabel] sizeWithAttributes:@{ @"NSFontAttributeName" : KK_CRYPTO_CHART_XAXIS_TEXT_FONT }].height - 2.0f;
    [path_x addLineToPoint:CGPointMake(x, lineHeight)];
    self.verticalLineLayer.path = path_x.CGPath;
    [self.verticalLineLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:2], nil]];
    [self.verticalLineLayer setStrokeColor:CROSS_SHAPED_CIRCLE_POINT_COLOR.CGColor];
    [self.layer insertSublayer:self.verticalLineLayer atIndex:10];

    //圆
    CGRect circleRect = CGRectMake(x-CROSS_SHAPED_CIRCLE_RADIUS/2, y-CROSS_SHAPED_CIRCLE_RADIUS/2, CROSS_SHAPED_CIRCLE_RADIUS, CROSS_SHAPED_CIRCLE_RADIUS);
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:circleRect cornerRadius:circleRect.size.width];
    self.circleLayer.path = circlePath.CGPath;
    [self.layer insertSublayer:self.circleLayer atIndex:10];
    
    //点
    CGRect pRect = CGRectMake(x-CROSS_SHAPED_CIRCLE_POINT_RADIUS/2, y-CROSS_SHAPED_CIRCLE_POINT_RADIUS/2, CROSS_SHAPED_CIRCLE_POINT_RADIUS, CROSS_SHAPED_CIRCLE_POINT_RADIUS);
    UIBezierPath *pPath = [UIBezierPath bezierPathWithRoundedRect:pRect cornerRadius:pRect.size.width];
    self.pointLayer.path = pPath.CGPath;
    [self.layer insertSublayer:self.pointLayer atIndex:10];
    [self.layer insertSublayer:self.infoLayer atIndex:10];
    [self changeInfoLayerFrame:x];
}

- (void)hideCrossLine {
    [self showCrossLine:NO];
}

- (void)showCrossLine:(BOOL)isShow{
    if (isShow) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
    }
    self.infoLayer.hidden = isShow == NO;
    
    self.pointLayer.hidden = isShow == NO;
    self.horizontalLineLayer.hidden = isShow == NO;
    self.verticalLineLayer.hidden = isShow == NO;
    self.circleLayer.hidden = isShow == NO;
    
    self.volumeLayer.hidden = isShow == NO;
    self.priceLayer.hidden = isShow == NO;
}

@end
