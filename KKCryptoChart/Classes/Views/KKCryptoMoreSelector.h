//
//  KKCryptoMoreSelector.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/10.
//

#define MORE_SELECTOR_COUNT 6

#import <UIKit/UIKit.h>
@class TabbarSelectUtil;

NS_ASSUME_NONNULL_BEGIN

@protocol KKCryptoMoreSelectorDelegate <NSObject>

@optional

// main chart selector changed
- (void)didMoreSelectorSelected:(NSInteger)type title:(NSString *)title;

@end

@interface KKCryptoMoreSelector : UIView

@property (assign, nonatomic) id<KKCryptoMoreSelectorDelegate> delegate;

- (void)setUpTypes:(NSArray<NSNumber *> *)types titles:(NSArray<NSString *> *)titles;
- (void)setAllBtnUnselected;

@end

NS_ASSUME_NONNULL_END
