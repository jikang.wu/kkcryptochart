//
//  NetworkRequestUtil.h
//  KikiChartsDemo
//
//  Created by apple on 2021/2/22.
//

#import <Foundation/Foundation.h>

@interface NetworkRequestUtil : NSObject

/*
 * fetch cloud data from specifical api
 */
+ (void)fetchCloudDataWithUrl:(NSString *)urlString completionHandler:(void (^)(NSDictionary * data, NSString * error))completionHandler;

@end

