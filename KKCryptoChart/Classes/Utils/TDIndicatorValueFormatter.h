//
//  TDIndicatorValueFormatter.h
//  KKCryptoChart
//
//  Created by apple on 2021/6/25.
//

#import <Foundation/Foundation.h>
#import "KKIndicatorModel.h"

@import KKCharts;
@interface TDIndicatorValueFormatter : NSObject <IChartValueFormatter>

@end

