//
//  KKCryptoChart.h
//  KKCryptoChart
//
//  Created by apple on 2021/3/12.
//

#import <Foundation/Foundation.h>
#import "KKCryptoChartView.h"
#import "KKCryptoChartGlobalConfig.h"
#import "KKCryptoChartStatusStorageUtil.h"

//! Project version number for KKCryptoChart.
FOUNDATION_EXPORT double KKCryptoChartVersionNumber;

//! Project version string for KKCryptoChart.
FOUNDATION_EXPORT const unsigned char KKCryptoChartVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KKCryptoChart/PublicHeader.h>


