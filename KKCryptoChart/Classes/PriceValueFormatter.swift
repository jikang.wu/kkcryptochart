//
//  PriceValueFormatter.swift
//  ChartsDemo
//  Copyright © 2016 dcg. All rights reserved.
//

import Foundation
import KKCharts

open class PriceValueFormatter: NSObject, IValueFormatter, IAxisValueFormatter
{

    @objc open var precision = 2
    
    public override init()
    {
        
    }
    
    @objc public init(precision: Int)
    {
        self.precision = precision
    }
    
    fileprivate func format(value: Double) -> String
    {
        let r = String(format: "%."+String(self.precision)+"f", value)
        return r
    }
    
    open func stringForValue(
        _ value: Double, axis: AxisBase?) -> String
    {
        return format(value: value)
    }
    
    open func stringForValue(
        _ value: Double,
        entry: ChartDataEntry,
        dataSetIndex: Int,
        viewPortHandler: ViewPortHandler?) -> String
    {
        return format(value: value)
    }
}
