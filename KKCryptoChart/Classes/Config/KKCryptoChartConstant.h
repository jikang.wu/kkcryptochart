//
//  KKCryptoChartConstant.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/2.
//

#ifndef KKCryptoChartConstant_h
#define KKCryptoChartConstant_h

#endif /* KKCryptoChartConstant_h */

#if DEBUG
#define KKLog(FORMAT, ...) NSLog(@"LOG >> Function:%s Line:%d Content:%@\n", __FUNCTION__, __LINE__, [NSString stringWithFormat:FORMAT, ##__VA_ARGS__])
#else
#define KKLog(FORMAT, ...)
#endif
//tab bar
#define TAB_BAR_BUTTON_HEIGHT 28

// axis line width
#define KK_CRYPTO_CHART_AXIS_WIDTH 1.0f

// axis line color
#define KK_CRYPTO_CHART_AXIS_COLOR [UIColor colorWithRed:216/255.f green:216/255.f blue:216/255.f alpha:1.0f]

// y axis text font
#define KK_CRYPTO_CHART_YAXIS_TEXT_FONT [UIFont systemFontOfSize:8]
// y axis text color
#define KK_CRYPTO_CHART_YAXIS_TEXT_COLOR [UIColor colorWithRed:142/255.f green:147/255.f blue:159/255.f alpha:1.0f]
#define KK_CRYPTO_CHART_YAXIS_WIDTH 50.f

// x axis text font
#define KK_CRYPTO_CHART_XAXIS_TEXT_FONT [UIFont systemFontOfSize:10]
// x axis text color
#define KK_CRYPTO_CHART_XAXIS_TEXT_COLOR [UIColor colorWithRed:105/255.f green:111/255.f blue:127/255.f alpha:1.0f]

// grid line width
#define KK_CRYPTO_CHART_GRID_LINE_WIDTH 0.5f

// grid line color
#define KK_CRYPTO_CHART_GRID_LINE_COLOR [UIColor colorWithRed:0.062f green:0.062f blue:0.062f alpha:1.0f]

// candle chart shadow line width
#define KK_CRYPTO_CHART_CANDLE_SHADOW_LINE_WIDTH 1.0f

// candle chart increase color
#define KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR [UIColor colorWithRed:30/255.f green:174/255.f blue:69/255.f alpha:1.0f]

// candle chart decrease color
#define KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR [UIColor colorWithRed:255/255.f green:55/255.f blue:80/255.f alpha:1.0f]

// candle chart neutral color
#define KK_CRYPTO_CHART_CANDLE_NEUTRAL_COLOR [UIColor colorWithRed:30/255.f green:174/255.f blue:69/255.f alpha:1.0f]

// indicator line color
#define KK_CRYPTO_CHART_LINE_1_COLOR [UIColor colorWithRed:255/255.f green:120/255.f blue:60/255.f alpha:1.f]
#define KK_CRYPTO_CHART_LINE_2_COLOR [UIColor colorWithRed:73/255.f green:165/255.f blue:255/255.f alpha:1.f]
#define KK_CRYPTO_CHART_LINE_3_COLOR [UIColor purpleColor]


// indicator button color
#define INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR [UIColor colorWithRed: 0.125f green:0.125f blue:0.125f alpha:0]
#define INDICATOR_BUTTON_SELECTED_BACKGROUND_COLOR [UIColor colorWithRed:0.945f green:0.494f blue:0 alpha:1.0f]
#define INDICATOR_BUTTON_DEFAULT_TEXT_COLOR [UIColor whiteColor]
#define INDICATOR_BUTTON_SELECTED_TEXT_COLOR [UIColor whiteColor]

// indicator default text color
#define INDICATOR_DEFAULT_DESCRIPTION_COLOR [UIColor colorWithRed:105/255.f green:111/255.f blue:127/255.f alpha:1.f]


// cross-shaped color circle radius
#define CROSS_SHAPED_LINE_COLOR [UIColor colorWithRed:204/255.f green:204/255.f blue:204/255.f alpha:1.f]
#define CROSS_SHAPED_CIRCLE_COLOR [UIColor colorWithRed:0.95f green:0.49f blue:0 alpha:0.4f]
#define CROSS_SHAPED_CIRCLE_POINT_COLOR [UIColor colorWithRed:0.95f green:0.49f blue:0 alpha:1.f]
#define CROSS_SHAPED_CIRCLE_RADIUS 9.5
#define CROSS_SHAPED_CIRCLE_POINT_RADIUS 2.5
#define AXIS_INFO_LAYER_X_OFFSET 2.0
#define AXIS_INFO_LAYER_Y_OFFSET 3.0

// price view color
#define KK_CRYPTO_CHART_PRICE_LINE_MIDDLE_COLOR [UIColor colorWithRed:0.95f green:0.49f blue:0 alpha:1.f]
#define KK_CRYPTO_CHART_PRICE_LINE_HIGH_COLOR [UIColor colorWithRed:30/255.f green:174/255.f blue:69/255.f alpha:1.0f]
#define KK_CRYPTO_CHART_PRICE_LINE_LOW_COLOR [UIColor colorWithRed:255/255.f green:55/255.f blue:80/255.f alpha:1.0f]
#define KK_CRYPTO_CHART_PRICE_BUTTON_TITLE_COLOR [UIColor whiteColor]

#define XAXIS_LABEL_COUNT 4

// frame
#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)

// web socket url
#define KK_WEB_SOCKET_BETA_URL @"ws://161.117.178.192:18090/"
#define KK_WEB_SOCKET_PROD_URL @"wss://ws.aspendigital.co:8080"
