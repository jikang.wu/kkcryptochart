//
//  main.m
//  KKCryptoChart
//
//  Created by HenryDang on 02/07/2022.
//  Copyright (c) 2022 HenryDang. All rights reserved.
//

@import UIKit;
#import "KKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KKAppDelegate class]));
    }
}
