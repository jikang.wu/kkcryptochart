//
//  KKViewController.m
//  KKCryptoChart
//
//  Created by HenryDang on 02/07/2022.
//  Copyright (c) 2022 HenryDang. All rights reserved.
//

#import "KKViewController.h"
#import <KKCryptoChart/KKCryptoChart.h>

@interface KKViewController ()

@property (strong, nonatomic) KKCryptoChartView *cryptoChartView;
@property (strong, nonatomic) UILabel *clearTabCacheLabel;
@property (strong, nonatomic) UISwitch *clearTabCacheSwitch;

@end

@implementation KKViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Combined Chart";
    [self initChartUI];
}

- (void)initChartUI {
    self.cryptoChartView = [[KKCryptoChartView alloc] initWithFrame:CGRectMake(0, 84, self.view.frame.size.width, 300)];
    [self.view addSubview:self.cryptoChartView];
    KKCryptoChartGlobalConfig *config = [[KKCryptoChartGlobalConfig alloc] initWithLocale:@"en" timeType:@"15" coinPrecision:@"2" environment:@"prod" coinCode:@"BTC_USDT"];
    self.cryptoChartView.config = config;
}


@end
